package fr.inria.lacodam;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import amie.data.KB;
import amie.data.KB.Instantiator;
import javatools.datatypes.Pair;
import javatools.datatypes.Triple;

/**
 * Small program that gets as input three TSV files:
 *  (1) a KB
 *  (2) a set of M triples
 *  (2) a set of N Horn rules
 *  (3) a boolean flag called strict. If false the program uses the confidence of rules
 *  to state a that the rule entails or not a triple prediction.
 *  
 * And returns a matrix of size M x N, where each entry states whether triple i
 * is entailed by rule j.  
 * 
 * @author lgalarra
 *
 */

public class RuleEvaluator {
	
	public static List<String[]> loadTriples(String fileName) {
		List<String[]> triples = null;
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			triples = stream.map(line -> line.split("\t")).collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return triples;
	}
	
	public static void main(String args[]) throws IOException {
		String kbFile = args[0];
		String triplesFile = args[1];
		String rulesFile = args[2];
		boolean strict = true;
		
		if (args.length > 3) {
			strict = Boolean.parseBoolean(args[3]);
		}
		
		// Load the KB
		KB kb = new KB();
		kb.load(new File(kbFile));
		
		// Load the triples
		List<String[]> triples = loadTriples(triplesFile);
		
		// Load the rules
		List<Triple<Pair<List<int[]>, int[]>, Long, Double>> rules = loadRules(rulesFile);
		for (String[] triple : triples) {
			System.out.println(evaluateTriple(triple, rules, kb, strict));
		}
	}

	private static String evaluateTriple(String[] triple, List<Triple<Pair<List<int[]>, int[]>, Long, Double>> rules,
			KB kb, boolean strict) {
		int[] intTriple = new int[] {KB.map(triple[0]), KB.map(triple[1]), KB.map(triple[2])};
		List<String> values = new ArrayList<>(); 
		for (Triple<Pair<List<int[]>, int[]>, Long, Double> rule : rules) {
			int[] head = rule.first.second;
			List<int[]> ruleBody = rule.first.first;
			// If one the constant position differs, then stop
			List<int[]> fullRule = new ArrayList<int[]>(ruleBody);
			List<int[]> bodyRule = deepClone(fullRule);
			fullRule.add(head);
			
			// If the triple is of the form <S, r, C> and the 
			// rule is of the form B => r(x, C') [x is a variable]
			if (differInConstantPosition(intTriple, head)) {
				// Now let us look if the antecedent is matched
				if (matches(bodyRule, kb, head, intTriple)) {
					// There is a disagreement
					// Now let us look at the polarity of the triple
					if (triple[1].contains("neg_")) {
						// We add a coefficient opposed to that polarity to state the disagreement
						values.add(strict ? "1" : "" + rule.third);
					} else {
						values.add(strict ? "-1" : "" + (-rule.third));						
					}
				} else {
					// Then the rule does not apply
					values.add("0");
				}
			} else {
				// There is no trivial contradiction
				if (matches(bodyRule, kb, head, intTriple)) {
					// The triple matches the rule's antecedent
					if (matches(fullRule, kb, head, intTriple)) {
						// The rule predicts the triple
						if (triple[1].contains("_neg")) {
							values.add(strict ? "-1" : "" + (-rule.third));							
						} else {
							values.add(strict ? "1" : "" + rule.third);														
						}
					} else {
						// We predict something else then. There is a disagreement
						if (triple[1].contains("_neg")) {
							values.add(strict ? "1" : "" + rule.third);							
						} else {
							values.add(strict ? "-1" : "" + (-rule.third));														
						}
					}
				} else {
					// Then the rule does not apply
					values.add("0");
				}
			}
		}
		
		return String.join("\t", values);

	}

	private static boolean matches(List<int[]> fullRule, KB kb, int[] head, int[] intTriple) {
		if (KB.numVariables(head) == 1) {
			int var1Pos = KB.firstVariablePos(head);
			int var1 = head[var1Pos];
			try(Instantiator insty1 = new Instantiator(fullRule, var1)) {
				insty1.instantiate(intTriple[var1Pos]);
				//System.out.println("Evaluating " + KB.toString(fullRule));
				return kb.existsBS1(fullRule);
			}
		} else {
			int var1Pos = KB.firstVariablePos(head);
			int var2Pos = KB.secondVariablePos(head);
			int var1 = head[var1Pos];
			int var2 = head[var2Pos];
			try(Instantiator insty1 = new Instantiator(fullRule, var1)) {
				insty1.instantiate(intTriple[var1Pos]);
				try (Instantiator insty2 = new Instantiator(fullRule, var2)) {
					insty2.instantiate(intTriple[var2Pos]);
					//System.out.println("Evaluating " + KB.toString(fullRule));
					return kb.existsBS1(fullRule);
				}
			}
		}
	}

	private static List<int[]> deepClone(List<int[]> fullRule) {
		List<int[]> result = new ArrayList<>();
		for (int[] elem : fullRule) {
			result.add(elem.clone());
		}
		
		return result;
	}

	private static boolean differInConstantPosition(int[] intTriple, int[] head) {
		for (int i = 0; i < intTriple.length; ++i) {
			if (!KB.isVariable(intTriple[i]) && !KB.isVariable(head[i])) {
				if (intTriple[i] != head[i]) {
					return true;
				}
			}
		}
		
		return false;
	}

	private static List<Triple<Pair<List<int[]>, int[]>, Long, Double>> loadRules(String fileName) {
		List<Triple<Pair<List<int[]>, int[]>, Long, Double>> rules = null;
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			rules = stream.map(line -> loadRule(line)).collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rules;
	}
	

	private static Triple<Pair<List<int[]>, int[]>, Long, Double> loadRule(String line) {
		String[] lineParts = line.split("\t");
		String ruleStr = lineParts[0];
		Pair<List<int[]>, int[]> rule = KB.rule(ruleStr);
		long support = Long.parseLong(lineParts[1]);
		double confidence = Double.parseDouble(lineParts[2]);
		
		return new Triple<>(rule, support, confidence);
	}

}
