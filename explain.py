#!/usr/bin/python3

"""
Gee Explain

Usage:
    explain list [--verbose|-v|-vv|-vvv]
    explain <dataset> <method> <latent_factors> <counter_examples> <rule_language> [options] [--verbose|-v|-vv|-vvv]
    explain -h | --help

Arguments:
    list                List the model(s) available for explanation computations.
    <dataset>           Any of the directory names under resources/data. The program automatically reads the file
                        train.tsv.
    <method>            hole|rescal|transe
    <latent_factors>    The dimension of the embeddings space (positive integer).
    <counter_examples>  The number of counter-examples generated per each example in train.tsv.
                        This value can be either an integer greater than 0, or a real number between 0 and 1.
                        E.g if this value is 0.2=1/5, then a counter-example will be generated for every 5 facts in
                        train.tsv.
    <rule_language>     pca-full-no-constant-rules|pca-full-rules
                        The first option mines rule with no constants.
                        The second allows for instantiated atoms in rules.

Options:
    -t=<threshold> --threshold=<threshold>  Coma-separated list of threshold values used to decide whether a black-box
                                            prediction is interpreted as a positive or negative prediction about the
                                            truth of a fact [default: 0.5,0.75,0.85,0.95].
    -l --local                              For local explanation computations.
    -g --global                             For global explanation computations.
    -r --recursive-rules                    If present, the body of the rules can contain the target predicate, allowing
                                            for rules such as marriedTo(x, y) -> __marriedTo(y, x).
                                            (The __ is a prefix that allows us to create a surrogate predicate that
                                            represents the verdicts of the black-box link predictor).
                        @todo : remove recursive rules : mark the difference between black-box and KG + do not prune
    --log-file=<path>                       If provided, the logs will (also) be written in this file.
    --quiet -q                              Do not print anything in STDOUT (to keep logs, please use --log-file)
                                            Please note that Critical/Error levels will still be outputted to STDERR.
    -v --verbose                            Display more or less information
                                            (0: Critical/Error, -v: Warning, -vv: Info, -vvv: Debug).
    -h --help                               Show this screen.
"""
import os
from typing import Union, Callable, List

from docopt import docopt
from sklearn.metrics import mean_squared_error

from data.kb import KB, get_training_kb
from explainers.discretization_functions import greater_than_expit_threshold, greater_than_median, set_threshold
from explainers.explainer import get_kb_explainer
from explainers.factory import get_model
from explainers.metrics import expit_mean_squared_error
from explainers.printer import output_scores, header
from utils.knowledge_graph import group_triples_by_predicate, sample_per_predicate
from utils.logger import configure_logger
from utils.misc import get_output_prefix

metrics_dict = {'hole': {'rules': expit_mean_squared_error, 'rules-linear': mean_squared_error}}


def run(kb: KB,
        method: str = 'hole',
        latent_factors: int = 10,
        n_counter_examples: Union[int, float] = 1,
        explainer: str = 'pca',
        counter_examples_per_predicate=None,
        positive_examples_fn: Callable = greater_than_median,
        output: os.PathLike = None,
        local_explanation: bool = False,
        global_explanation: bool = False
        ):
    """
    We compute an explanation for the ranking obtained by evaluating the whole test set by likelihood
    """
    # get the model & counter examples
    n, m, xs_train, id2entity, id2predicate = kb.fit()
    model, negative_examples = get_model(kb, method, latent_factors, n_counter_examples)

    # add counter examples to training set
    xs_train.extend(sample_per_predicate(negative_examples, counter_examples_per_predicate))

    test_filename = kb.get_data_root() + '/test.tsv'
    # load testing dataset
    xs_test = kb.fit_transform(os.path.abspath(test_filename))
    # group testing dataset by predicates
    test_dictionary = group_triples_by_predicate(xs_test)

    # instantiate explainer
    explainer = get_kb_explainer(explainer_name=explainer, model=model, kb=kb, context_instances=xs_train)

    # open output file and print header in it
    output_local = output_global = None
    if local_explanation:
        output_local = open(f'{output}_local.tsv', 'w', buffering=512)
        header(output_local)

    if global_explanation:
        output_global = open(f'{output}_global.tsv', 'w', buffering=512)
        header(output_global)

    # Start working loop
    for predicate_id in test_dictionary.keys():
        logger.info(f'Explaining triples for predicate "{kb.map_id_2_predicate(predicate_id)}" [{predicate_id}]')

        xs_test_predicate = test_dictionary[predicate_id]

        # explain globally
        if global_explanation:
            logger.debug('Compute global explanations.')
            global_explanations = explainer.explain_batch(xs_test_predicate, positive_examples_fn, recursive_rules=True)
            global_scores = global_explanations.scores()
            if global_scores is not None:
                translated_ex = ('', kb.map_id_2_predicate(predicate_id), '')
                output_scores(translated_ex, global_scores, global_explanations, file_wrapper=output_global)

        # explain locally
        if local_explanation:
            for example in xs_test_predicate:
                logger.debug('Compute local explanations.')
                translated_example = kb.convert_to_textual_triple(example)
                # Send the features to the explainer
                local_explanation = explainer.explain_instance(example, positive_examples_fn, recursive_rules=True)

                local_scores = local_explanation.scores()
                if local_scores is not None:
                    output_scores(translated_example, local_scores, local_explanation, output_local)

    # close the file IO wrapper
    if output_global is not None:
        output_global.close()
    if output_local is not None:
        output_local.close()


def run_explanations(kb: KB,
                     kb_name: str,
                     method: str,
                     latent_factors: int,
                     n_counter_examples: Union[int, float],
                     explainer: str,
                     global_explanation: bool = False,
                     local_explanation: bool = False,
                     thresholds: List[float] = None,
                     ):
    """
    For each threshold, for each (local|global), launch the explanations computations

    :param KB kb: The knowledge base we want to mine explanation from.
    :param str kb_name: The name of the used dataset (a folder in resources/data).
    :param str method: The embedding method used to predict new links.
    :param int latent_factors: The dimension of the embedding space.
    :param Union[int,float] n_counter_examples: The number of counter example that have to be generated, proportionally
        to the facts present in the KG.
    :param str explainer: todo
    :param bool global_explanation: If True, global explanation will (also) be mined.
    :param bool local_explanation: If True, local explanation will (also) be mined.
    :param List[float] thresholds: The list of thresholds for which explanations have to be computed.
    :return: Nothing
    """
    logger.debug('Beginning of explanation mining.')

    # Using None as default to prevent usage of mutable default parameters
    if thresholds is None:
        thresholds = [0.5, 0.75, 0.85, 0.95]

    for threshold in thresholds:
        if method != 'transe':
            fn = set_threshold(greater_than_expit_threshold, threshold)
        else:
            fn = set_threshold(greater_than_median, threshold)

        run(kb,
            method=method,
            latent_factors=latent_factors,
            n_counter_examples=n_counter_examples,
            explainer=explainer,
            counter_examples_per_predicate=100,
            positive_examples_fn=fn,
            output=get_output_prefix(kb_name, method, latent_factors, n_counter_examples, explainer, threshold),
            local_explanation=local_explanation,
            global_explanation=global_explanation
            )


if __name__ == '__main__':
    arguments = docopt(__doc__)

    logger = configure_logger(arguments['--verbose'], arguments['--log-file'], arguments['--quiet'])

    # display already computed models parameters
    if arguments['list']:
        logger.debug('The `list` command was selected.')
        model_path = 'resources/models/'
        model_path_files = [file for file in os.listdir(os.path.abspath(model_path)) if file.endswith('.pkl')]
        if len(model_path_files) == 0:
            print(f"There's no already computed model in {model_path}.")
        else:
            print(f"Here's a list of the computed models stored in {model_path}:")
            for f in [file.replace('.pkl', '').replace('_', ' ') for file in model_path_files]:
                print(f)
        exit(0)

    logger.debug('The `explain` command was selected.')

    # parse arguments
    # there is a 'g_' for 'global_', the idea is to avoid shadowing variables in the functions defined above
    g_thresholds = [float(x) for x in arguments['--threshold'].split(',')]

    # parse wanted explanation(s) mode(s)
    if not arguments['--global'] and not arguments['--local']:
        logger.error('No valid explanation mode was selected. Please look at the --local and/or --global options.')
        exit(1)

    # load KB
    g_kb = get_training_kb(os.path.join(os.path.abspath('resources/data/'), arguments['<dataset>']))
    counter_examples = int(arguments['<counter_examples>']) if str.isdigit(arguments['<counter_examples>']) else \
        float(arguments['<counter_examples>'])

    # launch the explanations
    run_explanations(
        g_kb,
        arguments['<dataset>'],
        arguments['<method>'],
        int(arguments['<latent_factors>']),
        counter_examples,
        arguments['<rule_language>'],
        global_explanation=arguments['--global'],
        local_explanation=arguments['--local'],
        thresholds=g_thresholds,
    )

    logger.debug('Explanation job exited normally')
