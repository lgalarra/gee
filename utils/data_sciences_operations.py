import math
import random

import numpy as np

from data.kb import PREDICATE, TRIPLE
from utils.logger import get_logger

logger = get_logger()


def split_train_test(input_data, relation_id=None, train=0.9):
    if train <= 0.0 or train >= 1.0:
        raise ValueError("'train' argument must be in the interval (0, 1)")
    if relation_id is None:
        data = input_data.copy()
    else:
        data = []
        for triple in input_data:
            if triple[TRIPLE][PREDICATE] == relation_id:
                data.append(triple)

    random.shuffle(data)
    train_idx = math.ceil(len(data) * train)

    train_data = data[:train_idx]
    test_data = data[train_idx:]

    return train_data, test_data


def normalize(an_array):
    min_val = np.min(an_array)
    delta = np.max(an_array) - min_val
    if delta > 0.0:
        return (an_array - min_val) / delta
    else:
        return an_array
