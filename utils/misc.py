import os
import random
import string

from utils.logger import get_logger

logger = get_logger()


def get_random_string(length: int) -> str:
    """
    Generate a random string with the combination of lower and upper case

    :param int length: The number of letters that the string have to contain.
    :return: The generated string.
    :rtype: str
    """
    letters = string.ascii_letters
    return ''.join([random.choice(letters) for _i in range(length)])


def get_output_prefix(kb_name, method, latent_factors, n_counter_examples, explainer, threshold):
    storage = f'experiments/{kb_name}'
    if not os.path.isdir(storage):
        logger.warning(f'Folder "{storage}" do not exist, creating it for ya.')
        os.makedirs(storage, exist_ok=True)
    file_name = f'{method}_{str(latent_factors)}_{str(n_counter_examples)}_{explainer}_{str(threshold)}'
    return os.path.abspath(f'{storage}/{file_name}')
