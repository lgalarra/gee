from typing import Union

from skge import TransE, HolE, RESCAL

Fact = tuple[int, int, int]
KGEModel = Union[RESCAL, HolE, TransE]
