import random
from typing import List

from skge.sample import LCWASampler

from data.kb import TRIPLE, PREDICATE, SUBJECT, OBJECT, TRUTH_VALUE
from utils.logger import get_logger
from utils.types import Fact

logger = get_logger()


def generate_negative_examples(input_data, sz):
    result = []
    test_sampler = LCWASampler(1, [0, 1], input_data, sz)
    for example in input_data:
        result.append(test_sampler.sample(example))

    return result


def get_subject_and_object_neighbors(instance: (Fact, int), xs: [(Fact, int)]) -> ([(Fact, int)], [(Fact, int)]):
    """
    Get the neighbors of the given instance in the given list of facts.

    This is a compilation of the two functions
        - get_same_subject_examples_neighbors
        - get_same_object_examples_neighbors
    """
    logger.debug(f'Looking for neighbors around {instance}.')
    subject_neighbors = []
    object_neighbors = []
    statement = instance[TRIPLE]
    for x in [x for x in xs if x[TRUTH_VALUE] == 1 and x[TRIPLE] != statement]:
        # If the subject is the same, add it
        if x[TRIPLE][SUBJECT] == statement[SUBJECT]:
            subject_neighbors.append(x)
        # If the object is the same, add it
        if x[TRIPLE][OBJECT] == statement[OBJECT]:
            object_neighbors.append(x)

    logger.debug(f'{len(subject_neighbors) + len(object_neighbors)} neighbors found.')
    return subject_neighbors, object_neighbors


def sample_per_predicate(examples: List[tuple[Fact, int]], n: int) -> List[tuple[Fact, int]]:
    """
    Sample `n` fact from the examples by unique predicate in the examples

    :param List[(Fact, int)] examples: The list of tuple Fact and truth score from which we want a sample.
    :param int n: The number of example by unique statement.
    :return: A sublist of examples, containing `n` element by unique statement
    :rtype: [(Fact, int)]
    """
    samples = []

    # prevent grouping `examples` if no counter example have to be generated
    n = int(n) if n is not None else 0
    if n == 0:
        return samples

    # group and sample
    predicate_map = group_triples_by_predicate(examples)

    for p, triples in predicate_map.items():
        length = len(triples)
        if length <= n:
            logger.error(
                f'Sample larger than population for predicate [{p}]. (Trying to get {n} elements out of {length})'
            )
            samples.extend(triples)
        else:
            samples.extend(list(random.sample(triples, n)))
    return samples


def group_triples_by_predicate(triples: List[tuple[Fact, int]]) -> dict[int, List[tuple[Fact, int]]]:
    """
    Group every triples by predicate in a map like this :
    ```
    {
        p1: [(s1,o1,p1), (s2,o2,p1), (s_i,o_i,p1)],
        p2: []
    }
    ```
    :param [(Fact, int)] triples: The given facts that have to be grouped, with their truth value.
    :return: The triples grouped by unique statement from the `triples` input.
    :rtype: dict[int, List[(Fact, int)]]
    """
    logger.debug('Grouping triples by predicate')
    predicates_map = {}
    for triple in triples:
        predicate = triple[TRIPLE][PREDICATE]
        if predicate not in predicates_map:
            predicates_map[predicate] = []

        predicates_map[predicate].append(triple)

    return predicates_map


def negate_predicate(predicate):
    if type(predicate) == int:
        return '<predicate_neg_' + str(predicate) + '>'
    elif type(predicate) == str and predicate.isnumeric():
        return '<predicate_neg_' + predicate + '>'
    elif predicate.startswith('<'):
        return predicate.replace('<', '<neg_')
    else:
        return 'neg_' + str(predicate)
