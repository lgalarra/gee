import sys
import csv
import re

def parse_rules(explanation_text) :
    if '---' in explanation_text :
        parts = explanation_text.split('---')
        working_text = parts[0].rstrip(' ')
        coeficients_part = parts[1].lstrip(' ').lstrip('[')
        end_coefs = coeficients_part.find(']')
        coeficients = [float(x) for x in coeficients_part[:end_coefs].split(';')]
    else :
        working_text = explanation_text
        coeficients = None

    clean_text = working_text.lstrip('[').rstrip(']')
    rules = clean_text.split('}, {')
    clean_rules = dict()
    for idx, rule in enumerate(rules) :
        clean_rule = rule.lstrip('{').rstrip('}')
        clean_rule_parts = clean_rule.split(',')
        clean_rule = clean_rule_parts[0].strip(' ')
        pattern = re.compile(r'(\?[c-z])')
        canonical_clean_rule = pattern.sub('?c', clean_rule)
        #print(clean_rule, canonical_clean_rule, file=sys.stderr)
        if coeficients is not None :
            clean_rules[canonical_clean_rule] = abs(coeficients[idx])
        else :
            ## Add the confidence
            clean_rules[canonical_clean_rule] = float(clean_rule_parts[2].replace('confidence: ', '').strip(' '))

    return clean_rules

def sort_by_coeficient(some_set, reference_dict) :
    list_of_pairs = []
    for rule in some_set :
        list_of_pairs.append((rule, reference_dict[rule]))

    return [x for x in sorted(list_of_pairs, key=lambda x : x[1], reverse=True)]

def compute_common_index_batch(file1, file2) :
    all_out_indexes = {'first-not-in-second' : {}, 'first-and-second' : {}, 'first' : {}, 'second': {} }
    with open(file1, 'r') as file1_obj :
        file1_csv = csv.DictReader(file1_obj, delimiter='\t')
        file1_dict = {}
        for row in file1_csv :
            explanation = row['explanation']
            file1_dict[row['Triple/Relation']] = parse_rules(explanation)

        file2_dict = {}
        with open(file2, 'r') as file2_obj :
            file2_csv = csv.DictReader(file2_obj, delimiter='\t')
            for row in file2_csv :
                key = row['Triple/Relation']
                explanation = row['explanation']
                file2_dict[key] = parse_rules(explanation)

        for predicate in file1_dict :
            rules1 = file1_dict[predicate]
            if predicate in file2_dict :
                rules2 = file2_dict[predicate]
            else :
                rules2 = {}

            all_out_indexes['first-and-second'][predicate] = sort_by_coeficient(rules1.keys() & rules2.keys(), rules1),
            all_out_indexes['first-not-in-second'][predicate] = sort_by_coeficient(rules1.keys() - rules2.keys(), rules1)
            all_out_indexes['first'][predicate] = sort_by_coeficient(rules1.keys(), rules1)
            all_out_indexes['second'][predicate] = sort_by_coeficient(rules2.keys(), rules1)

    return all_out_indexes

def update(predicate_dict, predicate, rules_dict) :
    if predicate not in predicate_dict :
        predicate_dict[predicate] = dict(rules_dict)
    else :
        #Now let us merge the dictionaries
        for rule in rules_dict :
            if rule not in predicate_dict[predicate] :
                predicate_dict[predicate][rule] = rules_dict[rule]
            else :
                predicate_dict[predicate][rule] += rules_dict[rule]

def compute_common_index_individual(file1, file2) :
    all_out_indexes = {'first-not-in-second': {}, 'first-and-second': {}, 'first' : {}, 'second': {}}
    with open(file1, 'r') as file1_obj:
        file1_csv = csv.DictReader(file1_obj, delimiter='\t')
        file1_dict = {}
        for row in file1_csv:
            triple = row['Triple/Relation']
            predicate = triple.split(' ')[1]
            explanation = row['explanation']
            update(file1_dict, predicate, parse_rules(explanation))

        file2_dict = {}
        with open(file2, 'r') as file2_obj:
            file2_csv = csv.DictReader(file2_obj, delimiter='\t')
            for row in file2_csv:
                triple = row['Triple/Relation']
                predicate = triple.split(' ')[1]
                explanation = row['explanation']
                update(file2_dict, predicate, parse_rules(explanation))

        for predicate in file1_dict:
            rules1 = file1_dict[predicate]
            if predicate in file2_dict:
                rules2 = file2_dict[predicate]
            else:
                rules2 = {}

            all_out_indexes['first-and-second'][predicate] = sort_by_coeficient(rules1.keys() & rules2.keys(), rules1),
            all_out_indexes['first-not-in-second'][predicate] = sort_by_coeficient(rules1.keys() - rules2.keys(),
                                                                                   rules1)
            all_out_indexes['first'][predicate] = sort_by_coeficient(rules1.keys(), rules1)
            all_out_indexes['second'][predicate] = sort_by_coeficient(rules2.keys(), rules2)

    return all_out_indexes

def parse_inverse_relations_file(ifile) :
    output_set = set()
    with open(ifile, 'r') as ifile_obj :
        reference_file_csv = csv.reader(ifile_obj, delimiter='\t')
        for row in reference_file_csv :
            r1 = '/' + row[0].lstrip('ns:').replace('.', '/')
            r2 = '/' + row[2].rstrip('.').lstrip('ns:').replace('.', '/')
            output_set.add(tuple((r1, r2)))
            output_set.add(tuple((r2, r1)))
    return output_set

def rule_inverse_bias(rule, inverse_relations_set) :
    bias = ''
    rule_text = rule[0]
    regex_rule = r'(/[./a-zA-Z0-9_]+)'
    match = re.findall(regex_rule, rule_text)
    relations = []
    for relation in match :
        relations.append(relation)
    if len(relations) != 2 :
        return ''
    r1 = relations[0]
    r2 = relations[1]
    if tuple((r1, r2)) in inverse_relations_set or tuple((r2, r1)) in inverse_relations_set :
        if 'inverse' not in bias :
            bias += 'inverse'
        ## I may add other biases

    return bias

## aggregated=true Then the input is assumed to be the aggregations per predicate
def rule_symmetric_bias(key, rules):
    searched_rule = '?b  ' + key + '  ?a   => ?a  __' + key + '  ?b'
    #print('Searching for ', searched_rule, 'in', rules[:10])
    try :
        idx = rules.index(searched_rule)
        return idx
    except ValueError :
        return -1

def compare_files(reference_file, file1, file2, inverse_relations_file=None, where='first-not-in-second') :
    print('Biases\tPredicate\tRules')
    if inverse_relations_file is not None :
        inverse_relations_pairs = parse_inverse_relations_file(inverse_relations_file)
    else :
        inverse_relations_pairs = None

    with open(reference_file, 'r') as reference_file_obj :
        reference_file_csv = csv.reader(reference_file_obj, delimiter='\t')
        if 'individual' in file1 :
            common_rule_index = compute_common_index_individual(file1, file2)
        else :
            common_rule_index = compute_common_index_batch(file1, file2)
        for row in reference_file_csv :
            key = row[0]
            bias = ''
            if key in common_rule_index[where] :
                first_idx = -1
                if inverse_relations_pairs is not None :
                    for idx, rule in enumerate(common_rule_index[where][key]) :
                        if rule[1] > 0.0 :
                           if bias == '' :
                               ibias = rule_inverse_bias(rule, inverse_relations_pairs)
                               bias += ibias
                               if ibias != '' :
                                   first_idx = idx

                idx_sym = rule_symmetric_bias(key, [x[0] for x in common_rule_index[where][key]])
                if idx_sym > -1 :
                    bias += ' symmetric'
                print(bias, key, (first_idx + 1), idx_sym + 1, common_rule_index[where][key], sep='\t')
                #print(bias, key, common_rule_index['first'], sep='\t')
                #print(key, len(list(filter(lambda x: x[1] > 0.0, common_rule_index['second'][key]))),
                #     len(common_rule_index['second'][key]), sep='\t', file=sys.stderr)
                #print(bias, key, common_rule_index['second'][key], sep='\t')
                #print(bias, key, sep='\t')

if __name__ == '__main__' :
    csv.field_size_limit(sys.maxsize)
    if len(sys.argv) > 4 :
        compare_files(sys.argv[1], sys.argv[2], sys.argv[3], inverse_relations_file=sys.argv[4], where='first')
    else :
        compare_files(sys.argv[1], sys.argv[2], sys.argv[3], where='first')