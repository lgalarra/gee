#!/bin/sh

python3 compare_rules.py ../resources/data/fb15k/stats_test.tsv ../experiments/fb15k/hole_200_1_pca-full-no-constant-rules_0.5_individual_linearm.tsv ../experiments/fb15k-237-wcp/hole_200_1_pca-full-no-constant-rules_0.5_individual_linearm.tsv > ../experiments/qualitative-experiments/comparison_hole_fb15k_individual.tsv

python3 compare_rules.py ../resources/data/fb15k/stats_test.tsv ../experiments/fb15k/rescal_150_1_pca-full-no-constant-rules_0.5_batch_linearm.tsv ../experiments/fb15k-237-wcp/rescal_150_1_pca-full-no-constant-rules_0.5_batch_linearm.tsv  > ../experiments/qualitative-experiments/comparison_rescal_fb15k_individual.tsv 

python3 compare_rules.py ../resources/data/wn18/stats_test.tsv ../experiments/wn18/hole_150_0.2_pca-full-no-constant-rules_0.5_individual_linearm.tsv ../experiments/wn18RR/hole_150_0.2_pca-full-no-constant-rules_0.5_individual_linearm.tsv  > ../experiments/qualitative-experiments/comparison_hole_wn18_individual.tsv 

python3 compare_rules.py ../resources/data/wn18/stats_test.tsv ../experiments/wn18/rescal_150_0.2_pca-full-no-constant-rules_0.5_individual_linearm.tsv ../experiments/wn18RR/rescal_150_0.1_pca-full-no-constant-rules_0.5_individual_linearm.tsv  > ../experiments/qualitative-experiments/comparison_rescal_wn18_individual.tsv
