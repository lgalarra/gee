import sys
import csv

root = sys.argv[1]
root = root.rstrip('/')
datasets = ['fb15k', 'fb15k-237-wcp', 'wn18', 'wn18RR', 'nell186']
#datasets = ['nell186']
for dataset in datasets :
    print('Computing stats for', dataset)
    with open(root + '/' + dataset + '/train.tsv', 'r') as somefile_obj:
        somefile_csv = csv.reader(somefile_obj, delimiter='\t')
        pred_dict = {}
        for row in somefile_csv :
            if row[1] not in pred_dict :
                pred_dict[row[1]] = 0

            pred_dict[row[1]] += 1
        with open(root + '/' + dataset + '/stats_train.tsv', 'w') as output :
            for pred in pred_dict :
                output.write(pred + '\t' + str(pred_dict[pred]) + '\n')