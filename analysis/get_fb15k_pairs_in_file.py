import sys
import csv

from analysis.compare_rules import parse_inverse_relations_file

predicates = set()
with open(sys.argv[1], 'r') as ifile_obj:
    reader = csv.reader(ifile_obj, delimiter='\t')
    for row in reader :
        predicate = row[1]
        predicates.add(predicate)

inverse_pairs = parse_inverse_relations_file(sys.argv[2])
unique_pairs = set()

for pair in inverse_pairs :
    if pair[0] in predicates and pair[1] in predicates :
        unique_pairs.add(tuple(sorted([pair[0], pair[1]])))

for pair in unique_pairs :
    print(pair)