import sys
import csv
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import os
from scipy.stats import pearsonr, spearmanr

csv.field_size_limit(sys.maxsize)

bestvaluesfn = {'bb-accuracy' : np.max, 'accuracy' : np.max, 'log-loss': np.min, 'auc-roc': np.max, 'mrr' : np.max,
                'mse' : np.min, 'r2' : np.max, 'mse-prob' : np.min, 'r2-prob' : np.max, 'target-accuracy' : np.max,
                'target-mr' : np.min, 'target-mse' : np.min, 'target-mse-prob' : np.min, 'coverage': np.max,
                'train-set' : np.min, 'n-rules' : np.max}

maxvalues = {'bb-accuracy' : 1.0, 'accuracy' : 1.0, 'log-loss': float('inf'), 'auc-roc': 1.0, 'mrr' : 1.0,
                'mse' : float('inf'), 'r2' : 1.0, 'mse-prob' : float('inf'), 'r2-prob' : 1.0, 'target-accuracy' : 1.0,
                'target-mr' : float('inf'), 'target-mse' : float('inf'), 'target-mse-prob' : float('inf'),
             'coverage' : 1.0, 'train-set' : float('inf'), 'n-rules' : float('inf')}

def extract_keys(somefilename) :
    components = somefilename.split('_')
    if len(components) < 7 :
        print('We could not extract information about filename', somefilename,
              file=sys.stderr)
        return None, None, None, None

    return components[6].replace('.tsv', ''), components[0], components[5], components[3]

# Returns all the files categorized by scope of the explanation,
# type and dataset
def get_files_dictionary(root) :
    ''' It returns a nested dictionary of the form:
    type (linear/binary) -> dataset (fb15k, etc..) -> black box (hole, rescal, etc..) ->
    scope (local/global) -> rules-type (no constants/constants) > filename'''
    if not root.endswith('/') :
        nroot = root + '/'
    else :
        nroot = root

    dictionary = dict()
    dataset = root.split('/')[-1]
    if dataset == '' :
        dataset = root.split('/')[-2]

    for path in Path(nroot).rglob('*.tsv'):
        filename = nroot + str(path.relative_to(nroot))
        extype, blackbox, scope, rulestype = extract_keys(path.name)

        if extype not in dictionary :
            dictionary[extype] = dict()

        if dataset not in dictionary[extype] :
            dictionary[extype][dataset] = dict()

        if blackbox not in dictionary[extype][dataset] :
            dictionary[extype][dataset][blackbox] = dict()

        if scope not in dictionary[extype][dataset][blackbox] :
            dictionary[extype][dataset][blackbox][scope] = dict()

        if rulestype not in dictionary[extype][dataset][blackbox][scope] :
            dictionary[extype][dataset][blackbox][scope][rulestype] = (list(), dict())

        dictionary[extype][dataset][blackbox][scope][rulestype][0].append(filename)

    return dictionary


def calculate_metrics_for_global_explanations(somefile, explanation_type, predicates_dict) :
    with open(somefile, 'r') as somefile_obj :
        somefile_csv = csv.DictReader(somefile_obj, delimiter='\t')
        if explanation_type == 'binary' :
            for row in somefile_csv :
                predicate = row['Triple/Relation']
                if predicate not in predicates_dict :
                    predicates_dict[predicate] = {'accuracy' : [], 'log-loss' : [],
                                                   'auc-roc' : [], 'context-mrr' : [],
                                                  'bb-accuracy' : [], 'mrr' : [], 'weights' : []}
                predicates_dict[predicate]['accuracy'].append(float(row['context-accuracy']))
                predicates_dict[predicate]['bb-accuracy'].append(float(row['bb-real-accuracy']))
                predicates_dict[predicate]['weights'].append(float(row['test-positive-set']) + float(row['test-negative-set']))
                predicates_dict[predicate]['log-loss'].append(float(row['context-log-loss']))
                predicates_dict[predicate]['auc-roc'].append(float(row['context-auc-roc']))
                predicates_dict[predicate]['mrr'].append(float(row['context-mrr']))

        else :
            for row in somefile_csv :
                predicate = row['Triple/Relation']
                if predicate not in predicates_dict:
                    predicates_dict[predicate] = {'mse' : [], 'r2' : [], 'accuracy' : [], 'auc-roc' : [],
                                                   'r2-prob' : [], 'mse-prob' : [], 'bb-accuracy' : [],
                                                  'weights' : [], 'mrr' : [], 'train-set' : [], 'n-rules' : []}
                predicates_dict[predicate]['accuracy'].append(float(row['context-accuracy']))
                predicates_dict[predicate]['bb-accuracy'].append(float(row['linear-real-accuracy']))
                predicates_dict[predicate]['auc-roc'].append(float(row['context-auc-roc']))
                predicates_dict[predicate]['mse'].append(float(row['context-mse']))
                predicates_dict[predicate]['weights'].append(float(row['test-positive-set']) + float(row['test-negative-set']))
                predicates_dict[predicate]['r2'].append(float(row['context-r2']))
                predicates_dict[predicate]['mse-prob'].append(float(row['context-mse-prob']))
                predicates_dict[predicate]['r2-prob'].append(float(row['context-r2-prob']))
                predicates_dict[predicate]['mrr'].append(float(row['context-mrr']))
                predicates_dict[predicate]['n-rules'].append(float(row['total-rules']))
                predicates_dict[predicate]['train-set'].append(float(row['train-positive-set']) + float(row['train-negative-set']))



def calculate_histogram_data_for_global_explanations(somefile, explanation_type, predicates_dict) :
    with open(somefile, 'r') as somefile_obj :
        somefile_csv = csv.DictReader(somefile_obj, delimiter='\t')
        if explanation_type == 'binary' :
            for row in somefile_csv :
                predicate = row['Triple/Relation']
                if predicate not in predicates_dict :
                    predicates_dict[predicate] = {'accuracy' : [], 'log-loss' : [],
                                                   'auc-roc' : [], 'context-mrr' : [],
                                                  'bb-accuracy' : [], 'mrr' : []}

                predicates_dict[predicate]['accuracy'].append(float(row['context-accuracy']))
                predicates_dict[predicate]['bb-accuracy'].append(float(row['bb-real-accuracy']))
                predicates_dict[predicate]['log-loss'].append(float(row['context-log-loss']))
                predicates_dict[predicate]['auc-roc'].append(float(row['context-auc-roc']))
                predicates_dict[predicate]['bb-accuracy'].append(float(row['bb-real-accuracy']))
                predicates_dict[predicate]['mrr'].append(float(row['context-mrr']))

        else :
            for row in somefile_csv:
                predicate = row['Triple/Relation']
                if predicate not in predicates_dict :
                    predicates_dict[predicate] = {'mse' : [], 'r2' : [], 'accuracy' : [],  'auc-roc' : [],
                                                   'r2-prob' : [], 'mse-prob' : [], 'mrr' : [], 'train-set' : [],
                                                  'bb-accuracy' : [], 'n-rules' : []}

                predicates_dict[predicate]['accuracy'].append(float(row['context-accuracy']))
                predicates_dict[predicate]['bb-accuracy'].append(float(row['linear-real-accuracy']))
                predicates_dict[predicate]['auc-roc'].append(float(row['context-auc-roc']))
                predicates_dict[predicate]['mse'].append(float(row['context-mse']))
                predicates_dict[predicate]['r2'].append(float(row['context-r2']))
                predicates_dict[predicate]['r2-prob'].append(float(row['context-r2-prob']))
                predicates_dict[predicate]['mse-prob'].append(float(row['context-mse-prob']))
                predicates_dict[predicate]['mrr'].append(float(row['context-mrr']))
                predicates_dict[predicate]['n-rules'].append(float(row['total-rules']))
                predicates_dict[predicate]['train-set'].append(float(row['train-positive-set']) + float(row['train-negative-set']))


def row_has_problem(row) :
    cols_to_look = ['context-mse', 'context-r2', 'context-mse-prob', 'context-r2-prob', 'target-mse', 'target-mse-prob']
    for col in cols_to_look :
        if col in row :
            if row[col] is None :
                return True
            parts = row[col].split('.')
            if 'e+' in row[col] or 'E+' in row[col] or len(parts[0]) >= 4 :
                return True

    return False


def calculate_metrics_for_local_explanations(somefile, explanation_type, predicates_dict, dataset_stats) :
    with open(somefile, 'r') as somefile_obj :
        somefile_csv = csv.DictReader(somefile_obj, delimiter='\t')
        rows_dict = {x : 0 for x in dataset_stats.keys()}
        
        if explanation_type == 'binary' :
            for row in somefile_csv:
                triple = row['Triple/Relation'].split(' ')
                predicate = triple[1]
                if predicate.startswith('neg_') :
                    predicate = predicate.lstrip('neg_')

                rows_dict[predicate] += 1

                if predicate not in predicates_dict:
                    predicates_dict[predicate] = {'accuracy': [], 'log-loss': [], 'target-accuracy' : [],
                                                  'auc-roc': [], 'context-mrr': [],
                                                  'bb-accuracy': [], 'mrr': [], 'target-mr' : [], 'auc-roc' : [],
                                                  'weights-data': {}, 'accuracy-data': {},
                                                  'target-accuracy-data' : {},
                                                  'log-loss-data': {}, 'auc-roc-data': {}, 'mrr-data': {},
                                                  'bb-accuracy-data': {}, 'mrr-data': {}, 'target-mr-data' : {},
                                                  'auc-roc-data' : {}, 'weights-data' : {}, 'coverage' : {}
                                                  }
                if somefile not in predicates_dict[predicate]['accuracy-data'] :
                    predicates_dict[predicate]['accuracy-data'][somefile] = []
                predicates_dict[predicate]['accuracy-data'][somefile].append(float(row['context-accuracy']))

                if somefile not in predicates_dict[predicate]['target-accuracy-data'] :
                    predicates_dict[predicate]['target-accuracy-data'][somefile] = []
                predicates_dict[predicate]['target-accuracy-data'][somefile].append(float(row['target-accuracy']))

                if somefile not in predicates_dict[predicate]['bb-accuracy-data'] :
                    predicates_dict[predicate]['bb-accuracy-data'][somefile] = []
                predicates_dict[predicate]['bb-accuracy-data'][somefile].append(float(row['bb-real-accuracy']))

                if somefile not in predicates_dict[predicate]['weights-data'] :
                    predicates_dict[predicate]['weights-data'][somefile] = []
                predicates_dict[predicate]['weights-data'][somefile].append( float(row['test-positive-set'])
                                                                        + float(row['test-negative-set']))

                if somefile not in predicates_dict[predicate]['log-loss-data'] :
                    predicates_dict[predicate]['log-loss-data'][somefile] = []
                predicates_dict[predicate]['log-loss-data'][somefile].append(float(row['context-log-loss']))

                if somefile not in predicates_dict[predicate]['auc-roc-data'] :
                    predicates_dict[predicate]['auc-roc-data'][somefile] = []
                predicates_dict[predicate]['auc-roc-data'][somefile].append(float(row['context-auc-roc']))

                if somefile not in predicates_dict[predicate]['mrr-data'] :
                    predicates_dict[predicate]['mrr-data'][somefile] = []
                predicates_dict[predicate]['mrr-data'][somefile].append(float(row['context-mrr']))

                if somefile not in predicates_dict[predicate]['target-mr-data'] :
                    predicates_dict[predicate]['target-mr-data'][somefile] = []
                predicates_dict[predicate]['target-mr-data'][somefile].append(float(row['target-mr']))

        else:
            for row in somefile_csv:
                if row_has_problem(row) :
                    continue

                triple = row['Triple/Relation'].split(' ')
                predicate = triple[1]
                if predicate.startswith('neg_'):
                    predicate = predicate.lstrip('neg_')

                if predicate not in predicates_dict:
                    predicates_dict[predicate] = {'mse': [], 'r2': [], 'accuracy' : [], 'auc-roc' : [], 'bb-accuracy': [],
                                                  'r2-prob': [], 'mse-prob': [], 'context-mrr': [], 'target-mse' : [], 'n-rules' : [],
                                                  'target-mse-prob': [], 'accuracy-data' : {}, 'weights-data': {},
                                                  'mse-data' : {}, 'r2-data' : {}, 'r2-prob-data' : {}, 'auc-roc-data' : {},
                                                  'mse-prob-data' : {}, 'target-mse-data' :{}, 'target-mse-prob-data' : {},
                                                  'mrr-data': {}, 'coverage' : {}, 'train-set-data' : {}, 'bb-accuracy-data': {},
                                                  'n-rules-data' : {} }



                if somefile not in predicates_dict[predicate]['mse-data'] :
                    predicates_dict[predicate]['mse-data'][somefile] = []

                predicates_dict[predicate]['mse-data'][somefile].append(float(row['context-mse']))

                if somefile not in predicates_dict[predicate]['accuracy-data'] :
                    predicates_dict[predicate]['accuracy-data'][somefile] = []
                predicates_dict[predicate]['accuracy-data'][somefile].append(float(row['context-accuracy']))

                if somefile not in predicates_dict[predicate]['bb-accuracy-data'] :
                    predicates_dict[predicate]['bb-accuracy-data'][somefile] = []
                predicates_dict[predicate]['bb-accuracy-data'][somefile].append(float(row['linear-real-accuracy']))

                if somefile not in predicates_dict[predicate]['auc-roc-data'] :
                    predicates_dict[predicate]['auc-roc-data'][somefile] = []
                predicates_dict[predicate]['auc-roc-data'][somefile].append(float(row['context-auc-roc']))

                if somefile not in predicates_dict[predicate]['weights-data']:
                    predicates_dict[predicate]['weights-data'][somefile] = []
                predicates_dict[predicate]['weights-data'][somefile].append(float(row['test-positive-set'])
                                                                            + float(row['test-negative-set']))

                if somefile not in predicates_dict[predicate]['train-set-data']:
                    predicates_dict[predicate]['train-set-data'][somefile] = []
                predicates_dict[predicate]['train-set-data'][somefile].append(float(row['train-positive-set'])
                                                                            + float(row['train-negative-set']))

                if somefile not in predicates_dict[predicate]['r2-data']:
                    predicates_dict[predicate]['r2-data'][somefile] = []
                predicates_dict[predicate]['r2-data'][somefile].append(float(row['context-r2']))

                if somefile not in predicates_dict[predicate]['mse-prob-data']:
                    predicates_dict[predicate]['mse-prob-data'][somefile] = []
                predicates_dict[predicate]['mse-prob-data'][somefile].append(float(row['context-mse-prob']))

                if somefile not in predicates_dict[predicate]['r2-prob-data']:
                    predicates_dict[predicate]['r2-prob-data'][somefile] = []
                predicates_dict[predicate]['r2-prob-data'][somefile].append(float(row['context-r2-prob']))

                if somefile not in predicates_dict[predicate]['target-mse-data'] :
                    predicates_dict[predicate]['target-mse-data'][somefile] = []
                predicates_dict[predicate]['target-mse-data'][somefile].append(float(row['target-mse']))

                if somefile not in predicates_dict[predicate]['target-mse-prob-data'] :
                    predicates_dict[predicate]['target-mse-prob-data'][somefile] = []
                predicates_dict[predicate]['target-mse-prob-data'][somefile].append(float(row['target-mse-prob']))

                if somefile not in predicates_dict[predicate]['mrr-data'] :
                    predicates_dict[predicate]['mrr-data'][somefile] = []
                predicates_dict[predicate]['mrr-data'][somefile].append(float(row['context-mrr']))

                if somefile not in predicates_dict[predicate]['n-rules-data']:
                    predicates_dict[predicate]['n-rules-data'][somefile] = []
                predicates_dict[predicate]['n-rules-data'][somefile].append(float(row['total-rules']))
                
                rows_dict[predicate] += 1

        for predicate in predicates_dict :
            predicates_dict[predicate]['coverage'][somefile] = rows_dict[predicate] / dataset_stats[predicate]



def calculate_histogram_data_for_local_explanations(somefile, explanation_type, predicates_dict) :
    return {}

def calculate_metrics_for_file(somefile, explanation_type, scope, predicates_dict, dataset_stats) :
    if scope == 'batch' :
        return calculate_metrics_for_global_explanations(somefile, explanation_type, predicates_dict)
    else :
        return calculate_metrics_for_local_explanations(somefile, explanation_type, predicates_dict, dataset_stats)

def calculate_histogram_data_for_file(somefile, explanation_type, scope, predicates_dict) :
    if scope == 'batch' :
        return calculate_histogram_data_for_global_explanations(somefile, explanation_type, predicates_dict)
    else :
        return calculate_histogram_data_for_local_explanations(somefile, explanation_type, predicates_dict)

def calculate_metrics_for_all_files(files_dict, dataset_stats) :
    for extype, extypedict in files_dict.items() :
        for dataset, datasetdict in extypedict.items() :
            for blackbox, blackboxdict in datasetdict.items() :
                for scope, scopedict in blackboxdict.items() :
                    for rulestype, rulestypedict in scopedict.items() :
                        (file_list, metrics_dict) = rulestypedict
                        for filename in file_list :
                            calculate_metrics_for_file(filename, extype, scope, metrics_dict, dataset_stats)
                        if scope == 'batch' :
                            aggregate_metrics_dict_global_explanations(metrics_dict, dataset_stats, extype)
                        else :
                            aggregate_metrics_dict_local_explanations(metrics_dict, dataset_stats, extype)

def aggregate_metric_values_from_different_files(dict_per_file_and_predicate) :
    aggs = []
    for file in dict_per_file_and_predicate :
        aggs.append(np.mean(dict_per_file_and_predicate[file]))

    return aggs

def aggregate_metrics_dict_local_explanations(metrics_dict, dataset_stats, extype ='binary') :
    if extype == 'binary' :
        context_accuracies = []
        target_accuracies = []
        blackbox_accuracies = []
        log_losses = []
        auc_rocs = []
        mrrs = []
        target_mrrs = []
        coverages = []

        weights_accuracy = []
        weights_target_accuracy = []
        weights_bb_accuracy = []
        weights_log_loss = []
        weights_auc_roc = []
        weights_mrr = []
        weights_target_mrr = []
        weights_coverage = []

        for predicate in metrics_dict :
            metrics_dict[predicate]['weights'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['weights-data'])

            metrics_dict[predicate]['accuracy'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['accuracy-data'])
            best_accuracy = bestvaluesfn['accuracy'](metrics_dict[predicate]['accuracy'])
            idx_accuracy = metrics_dict[predicate]['accuracy'].index(best_accuracy)
            weights_accuracy.append(metrics_dict[predicate]['weights'][idx_accuracy])
            context_accuracies.append(best_accuracy)

            metrics_dict[predicate]['target-accuracy'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['target-accuracy-data'])
            best_target_accuracy = bestvaluesfn['target-accuracy'](metrics_dict[predicate]['target-accuracy'])
            idx_target_accuracy = metrics_dict[predicate]['target-accuracy'].index(best_target_accuracy)
            weights_target_accuracy.append(metrics_dict[predicate]['weights'][idx_target_accuracy])
            target_accuracies.append(best_target_accuracy)

            metrics_dict[predicate]['bb-accuracy'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['bb-accuracy-data'])
            best_bbaccuracy = bestvaluesfn['bb-accuracy'](metrics_dict[predicate]['bb-accuracy'])
            idx_bbaccuracy = metrics_dict[predicate]['bb-accuracy'].index(best_bbaccuracy)
            weights_bb_accuracy.append(metrics_dict[predicate]['weights'][idx_bbaccuracy])
            blackbox_accuracies.append(best_bbaccuracy)

            metrics_dict[predicate]['log-loss'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['log-loss-data'])
            best_logloss = bestvaluesfn['log-loss'](metrics_dict[predicate]['log-loss'])
            idx_logloss = metrics_dict[predicate]['log-loss'].index(best_logloss)
            weights_log_loss.append(metrics_dict[predicate]['weights'][idx_logloss])
            log_losses.append(best_logloss)

            metrics_dict[predicate]['auc-roc'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['auc-roc-data'])
            best_aucroc = bestvaluesfn['auc-roc'](metrics_dict[predicate]['auc-roc'])
            idx_aucroc = metrics_dict[predicate]['auc-roc'].index(best_aucroc)
            weights_auc_roc.append(metrics_dict[predicate]['weights'][idx_aucroc])
            auc_rocs.append(best_aucroc)

            metrics_dict[predicate]['mrr'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['mrr-data'])
            best_mrr = bestvaluesfn['mrr'](metrics_dict[predicate]['mrr'])
            idx_mrr = metrics_dict[predicate]['mrr'].index(best_mrr)
            weights_mrr.append(metrics_dict[predicate]['weights'][idx_mrr])
            mrrs.append(best_mrr)

            metrics_dict[predicate]['target-mr'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['target-mr-data'])
            best_target_mrr = bestvaluesfn['target-mr'](metrics_dict[predicate]['target-mr'])
            idx_target_mrr = metrics_dict[predicate]['target-mr'].index(best_target_mrr)
            weights_target_mrr.append(metrics_dict[predicate]['weights'][idx_target_mrr])
            target_mrrs.append(best_target_mrr)
            
            metrics_dict[predicate]['coverage'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['coverage'])
            best_coverage = bestvaluesfn['coverage'](metrics_dict[predicate]['coverage'])
            weights_coverage.append(dataset_stats[predicate])
            coverages.append(best_coverage)

        uncovered_predicates = set(dataset_stats.keys()) - set(metrics_dict.keys())
        for up in uncovered_predicates :
            coverages.append(0.0)
            weights_coverage.append(dataset_stats[up]) 

        if len(context_accuracies) > 0:
            metrics_dict['aggregation'] = {}
            metrics_dict['aggregation']['accuracy'] = np.average(context_accuracies, weights=weights_accuracy)
            metrics_dict['aggregation']['accuracy-stdev'] = np.std(context_accuracies)
            metrics_dict['aggregation']['accuracy-max'] = np.max(context_accuracies)
            metrics_dict['aggregation']['target-accuracy'] = np.average(target_accuracies, weights=weights_target_accuracy)
            metrics_dict['aggregation']['target-accuracy-stdev'] = np.std(target_accuracies)
            metrics_dict['aggregation']['target-accuracy-max'] = np.max(target_accuracies)
            metrics_dict['aggregation']['black-box-accuracy'] = np.average(blackbox_accuracies, weights=weights_bb_accuracy)
            metrics_dict['aggregation']['black-box-accuracy-stdev'] = np.std(blackbox_accuracies)
            metrics_dict['aggregation']['black-box-accuracy-max'] = np.max(blackbox_accuracies)
            metrics_dict['aggregation']['log-loss'] = np.average(log_losses, weights=weights_log_loss)
            metrics_dict['aggregation']['log-loss-stdev'] = np.std(log_losses)
            metrics_dict['aggregation']['log-loss-min'] = np.min(log_losses)
            metrics_dict['aggregation']['auc-roc'] = np.average(auc_rocs, weights=weights_auc_roc)
            metrics_dict['aggregation']['auc-roc-stdev'] = np.std(auc_rocs)
            metrics_dict['aggregation']['auc-roc-max'] = np.max(auc_rocs)
            metrics_dict['aggregation']['mrr'] = np.average(mrrs, weights=weights_mrr)
            metrics_dict['aggregation']['mrr-stdev'] = np.std(mrrs)
            metrics_dict['aggregation']['mrr-max'] = np.max(mrrs)
            metrics_dict['aggregation']['target-mrr'] = np.average(target_mrrs, weights=weights_target_mrr)
            metrics_dict['aggregation']['target-mrr-stdev'] = np.std(target_mrrs)
            metrics_dict['aggregation']['target-mrr-max'] = np.max(target_mrrs)
            metrics_dict['aggregation']['n_preds'] = len(metrics_dict.keys())
            metrics_dict['aggregation']['coverage'] = np.average(coverages, weights=weights_coverage)

    else :
        context_mse = []
        target_mse = []
        r2s = []
        context_mse_prob = []
        target_mse_prob = []
        r2s_prob = []
        n_preds = 0
        coverages = []
        mrrs = []
        context_accuracies = []
        auc_rocs = []
        train_sets = []
        blackbox_accuracies = []
        n_rules = []



        weights_mse = []
        weights_target_mse = []
        weights_r2 = []
        weights_r2_prob = []
        weights_mse_prob = []
        weights_target_mse_prob = []
        weights_mrr = []
        weights_coverage = []
        weights_accuracy = []
        weights_auc_roc = []
        weights_bb_accuracy = []
        weights_n_rules = []


        covered_predicates = set()
        for predicate in metrics_dict:
            metrics_dict[predicate]['weights'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['weights-data'])

            if metrics_dict[predicate]['mse-data'] == {} :
                continue

            metrics_dict[predicate]['mse'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['mse-data'])
            best_mse = bestvaluesfn['mse'](metrics_dict[predicate]['mse'])
            idx_mse = metrics_dict[predicate]['mse'].index(best_mse)
            weights_mse.append(metrics_dict[predicate]['weights'][idx_mse])
            context_mse.append(best_mse)

            metrics_dict[predicate]['target-mse'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['target-mse-data'])
            best_target_mse = bestvaluesfn['target-mse'](metrics_dict[predicate]['target-mse'])
            idx_target_mse = metrics_dict[predicate]['target-mse'].index(best_target_mse)
            weights_target_mse.append(metrics_dict[predicate]['weights'][idx_target_mse])
            target_mse.append(best_target_mse)


            metrics_dict[predicate]['accuracy'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['accuracy-data'])
            best_accuracy = bestvaluesfn['accuracy'](metrics_dict[predicate]['accuracy'])
            idx_accuracy = metrics_dict[predicate]['accuracy'].index(best_accuracy)
            weights_accuracy.append(metrics_dict[predicate]['weights'][idx_accuracy])
            context_accuracies.append(best_accuracy)

            metrics_dict[predicate]['bb-accuracy'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['bb-accuracy-data'])
            best_bbaccuracy = bestvaluesfn['bb-accuracy'](metrics_dict[predicate]['bb-accuracy'])
            idx_bbaccuracy = metrics_dict[predicate]['bb-accuracy'].index(best_bbaccuracy)
            weights_bb_accuracy.append(metrics_dict[predicate]['weights'][idx_bbaccuracy])
            blackbox_accuracies.append(best_bbaccuracy)

            metrics_dict[predicate]['auc-roc'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['auc-roc-data'])
            best_aucroc = bestvaluesfn['auc-roc'](metrics_dict[predicate]['auc-roc'])
            idx_aucroc = metrics_dict[predicate]['auc-roc'].index(best_aucroc)
            weights_auc_roc.append(metrics_dict[predicate]['weights'][idx_aucroc])
            auc_rocs.append(best_aucroc)

            metrics_dict[predicate]['r2'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['r2-data'])
            best_r2 = bestvaluesfn['r2'](metrics_dict[predicate]['r2'])
            idx_r2 = metrics_dict[predicate]['r2'].index(best_r2)
            weights_r2.append(metrics_dict[predicate]['weights'][idx_r2])
            r2s.append(best_r2)

            metrics_dict[predicate]['r2-prob'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['r2-prob-data'])
            best_r2_prob = bestvaluesfn['r2-prob'](metrics_dict[predicate]['r2-prob'])
            idx_r2_prob = metrics_dict[predicate]['r2-prob'].index(best_r2_prob)
            weights_r2_prob.append(metrics_dict[predicate]['weights'][idx_r2_prob])
            r2s_prob.append(best_r2_prob)

            metrics_dict[predicate]['target-mse-prob'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['target-mse-prob-data'])
            best_target_mse_prob = bestvaluesfn['target-mse-prob'](metrics_dict[predicate]['target-mse-prob'])
            idx_target_mse_prob = metrics_dict[predicate]['target-mse-prob'].index(best_target_mse_prob)
            weights_target_mse_prob.append(metrics_dict[predicate]['weights'][idx_target_mse_prob])
            target_mse_prob.append(best_target_mse_prob)

            metrics_dict[predicate]['mse-prob'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['mse-prob-data'])
            best_mse_prob = bestvaluesfn['mse-prob'](metrics_dict[predicate]['mse-prob'])
            idx_mse_prob = metrics_dict[predicate]['mse-prob'].index(best_mse_prob)
            weights_mse_prob.append(metrics_dict[predicate]['weights'][idx_mse_prob])
            context_mse_prob.append(best_mse_prob)

            metrics_dict[predicate]['mrr'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['mrr-data'])
            best_mrr = bestvaluesfn['mrr'](metrics_dict[predicate]['mrr'])
            idx_mrr = metrics_dict[predicate]['mrr'].index(best_mrr)
            weights_mrr.append(metrics_dict[predicate]['weights'][idx_mrr])
            mrrs.append(best_mrr)

            metrics_dict[predicate]['coverage'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['coverage'])
            best_coverage = bestvaluesfn['coverage'](metrics_dict[predicate]['coverage'])
            weights_coverage.append(dataset_stats[predicate])
            coverages.append(best_coverage)

            metrics_dict[predicate]['n-rules'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['n-rules-data'])
            best_n_rules = bestvaluesfn['n-rules'](metrics_dict[predicate]['n-rules'])
            idx_n_rules = metrics_dict[predicate]['n-rules'].index(best_n_rules)
            weights_n_rules.append(metrics_dict[predicate]['weights'][idx_n_rules])
            n_rules.append(best_n_rules)

            metrics_dict[predicate]['train-set'] = aggregate_metric_values_from_different_files(
                metrics_dict[predicate]['train-set-data']
            )
            ts_pos = bestvaluesfn['train-set'](metrics_dict[predicate]['train-set'])
            train_sets.append(ts_pos)

            covered_predicates.add(predicate)

        uncovered_predicates = set(dataset_stats.keys()) - covered_predicates
        for up in uncovered_predicates :
            coverages.append(0.0)
            weights_coverage.append(dataset_stats[up]) 

        if len(context_mse) > 0:
            metrics_dict['aggregation'] = {}
            metrics_dict['aggregation']['accuracy'] = np.average(context_accuracies, weights=weights_accuracy)
            metrics_dict['aggregation']['black-box-accuracy'] = np.average(blackbox_accuracies, weights=weights_bb_accuracy)
            metrics_dict['aggregation']['auc-roc'] = np.average(auc_rocs, weights=weights_auc_roc)
            metrics_dict['aggregation']['mse'] = np.average(context_mse, weights=weights_mse)
            metrics_dict['aggregation']['mse-stdev'] = np.std(context_mse)
            metrics_dict['aggregation']['mse-stdev'] = np.min(context_mse)
            metrics_dict['aggregation']['target-mse'] = np.average(target_mse, weights=weights_target_mse)
            metrics_dict['aggregation']['target-mse-stdev'] = np.std(target_mse)
            metrics_dict['aggregation']['target-mse-stdev'] = np.min(target_mse)
            metrics_dict['aggregation']['n_preds'] = n_preds
            metrics_dict['aggregation']['r2'] = np.average(r2s, weights=weights_r2)
            metrics_dict['aggregation']['r2-stdev'] = np.std(r2s)
            metrics_dict['aggregation']['r2-max'] = np.max(r2s)
            metrics_dict['aggregation']['r2-prob'] = np.average(r2s_prob, weights=weights_r2_prob)
            metrics_dict['aggregation']['r2-prob-stdev'] = np.std(r2s_prob)
            metrics_dict['aggregation']['r2-prob-max'] = np.max(r2s_prob)
            metrics_dict['aggregation']['mse-prob'] = np.average(context_mse_prob, weights=weights_mse_prob)
            metrics_dict['aggregation']['mse-prob-stdev'] = np.std(context_mse_prob)
            metrics_dict['aggregation']['mse-prob-stdev'] = np.min(context_mse_prob)
            metrics_dict['aggregation']['target-mse-prob'] = np.average(target_mse_prob, weights=weights_target_mse_prob)
            metrics_dict['aggregation']['target-mse-prob-stdev'] = np.std(target_mse_prob)
            metrics_dict['aggregation']['target-mse-prob-stdev'] = np.min(target_mse_prob)
            metrics_dict['aggregation']['mrr'] = np.average(mrrs, weights=weights_mrr)
            metrics_dict['aggregation']['mrr-stdev'] = np.std(mrrs)
            metrics_dict['aggregation']['mrr-max'] = np.max(mrrs)
            metrics_dict['aggregation']['coverage'] = np.average(coverages, weights=weights_coverage)
            metrics_dict['aggregation']['train-set'] = np.average(train_sets)
            metrics_dict['aggregation']['n-rules'] = np.average(n_rules, weights=weights_n_rules)


def aggregate_metrics_dict_global_explanations(metrics_dict, dataset_stats, extype ='binary') :
    if extype == 'binary' :
        context_accuracies = []
        blackbox_accuracies = []
        log_losses = []
        auc_rocs = []
        mrrs = []

        weights_accuracy = []
        weights_bb_accuracy = []
        weights_log_loss = []
        weights_auc_roc = []
        weights_mrr = []

        for predicate in metrics_dict :
            best_accuracy = bestvaluesfn['accuracy'](metrics_dict[predicate]['accuracy'])
            idx_accuracy = metrics_dict[predicate]['accuracy'].index(best_accuracy)
            weights_accuracy.append(metrics_dict[predicate]['weights'][idx_accuracy])
            context_accuracies.append(best_accuracy)

            best_bbaccuracy = bestvaluesfn['bb-accuracy'](metrics_dict[predicate]['bb-accuracy'])
            idx_bbaccuracy = metrics_dict[predicate]['bb-accuracy'].index(best_bbaccuracy)
            weights_bb_accuracy.append(metrics_dict[predicate]['weights'][idx_bbaccuracy])
            blackbox_accuracies.append(best_bbaccuracy)

            best_logloss = bestvaluesfn['log-loss'](metrics_dict[predicate]['log-loss'])
            idx_logloss = metrics_dict[predicate]['log-loss'].index(best_logloss)
            weights_log_loss.append(metrics_dict[predicate]['weights'][idx_logloss])
            log_losses.append(best_logloss)

            best_aucroc = bestvaluesfn['auc-roc'](metrics_dict[predicate]['auc-roc'])
            idx_aucroc = metrics_dict[predicate]['auc-roc'].index(best_aucroc)
            weights_auc_roc.append(metrics_dict[predicate]['weights'][idx_aucroc])
            auc_rocs.append(best_aucroc)

            best_mrr = bestvaluesfn['mrr'](metrics_dict[predicate]['mrr'])
            idx_mrr = metrics_dict[predicate]['mrr'].index(best_mrr)
            weights_mrr.append(metrics_dict[predicate]['weights'][idx_mrr])
            mrrs.append(best_mrr)

        if len(context_accuracies) > 0:
            metrics_dict['aggregation'] = {}
            metrics_dict['aggregation']['accuracy'] = np.average(context_accuracies, weights=weights_accuracy)
            metrics_dict['aggregation']['accuracy-stdev'] = np.std(context_accuracies)
            metrics_dict['aggregation']['accuracy-max'] = np.max(context_accuracies)
            metrics_dict['aggregation']['black-box-accuracy'] = np.average(blackbox_accuracies, weights=weights_bb_accuracy)
            metrics_dict['aggregation']['black-box-accuracy-stdev'] = np.std(blackbox_accuracies)
            metrics_dict['aggregation']['black-box-accuracy-max'] = np.max(blackbox_accuracies)
            metrics_dict['aggregation']['log-loss'] = np.average(log_losses, weights=weights_log_loss)
            metrics_dict['aggregation']['log-loss-stdev'] = np.std(log_losses)
            metrics_dict['aggregation']['log-loss-min'] = np.min(log_losses)
            metrics_dict['aggregation']['auc-roc'] = np.average(auc_rocs, weights=weights_auc_roc)
            metrics_dict['aggregation']['auc-roc-stdev'] = np.std(auc_rocs)
            metrics_dict['aggregation']['auc-roc-max'] = np.max(auc_rocs)
            metrics_dict['aggregation']['mrr'] = np.average(mrrs, weights=weights_mrr)
            metrics_dict['aggregation']['mrr-stdev'] = np.std(mrrs)
            metrics_dict['aggregation']['mrr-max'] = np.max(mrrs)
            metrics_dict['aggregation']['n_preds'] = len(metrics_dict.keys())
            total_coverage = 0
            coverage = 0
            for predicate in dataset_stats :
                total_coverage += dataset_stats[predicate]
                if predicate in metrics_dict :
                    coverage += dataset_stats[predicate]

            metrics_dict['aggregation']['coverage'] = coverage / total_coverage
    else :
        context_mse = []
        r2s = []
        context_mse_prob = []
        r2s_prob = []
        mrrs = []
        context_accuracies = []
        n_preds = 0
        auc_rocs = []
        train_sets = []
        blackbox_accuracies = []
        n_rules = []


        weights_mse = []
        weights_r2 = []
        weights_r2_prob = []
        weights_mse_prob = []
        weights_mrr = []
        weights_accuracy = []
        weights_auc_roc = []
        weights_bb_accuracy = []
        weights_n_rules = []

        for predicate in metrics_dict :
            best_accuracy = bestvaluesfn['accuracy'](metrics_dict[predicate]['accuracy'])
            idx_accuracy = metrics_dict[predicate]['accuracy'].index(best_accuracy)
            weights_accuracy.append(metrics_dict[predicate]['weights'][idx_accuracy])
            context_accuracies.append(best_accuracy)

            best_bbaccuracy = bestvaluesfn['bb-accuracy'](metrics_dict[predicate]['bb-accuracy'])
            idx_bbaccuracy = metrics_dict[predicate]['bb-accuracy'].index(best_bbaccuracy)
            weights_bb_accuracy.append(metrics_dict[predicate]['weights'][idx_bbaccuracy])
            blackbox_accuracies.append(best_bbaccuracy)

            best_aucroc = bestvaluesfn['auc-roc'](metrics_dict[predicate]['auc-roc'])
            idx_aucroc = metrics_dict[predicate]['auc-roc'].index(best_aucroc)
            weights_auc_roc.append(metrics_dict[predicate]['weights'][idx_aucroc])
            auc_rocs.append(best_aucroc)

            best_mse = bestvaluesfn['mse'](metrics_dict[predicate]['mse'])
            idx_mse = metrics_dict[predicate]['mse'].index(best_mse)
            weights_mse.append(metrics_dict[predicate]['weights'][idx_mse])
            context_mse.append(best_mse)

            best_r2 = bestvaluesfn['r2'](metrics_dict[predicate]['r2'])
            idx_r2 = metrics_dict[predicate]['r2'].index(best_r2)
            weights_r2.append(metrics_dict[predicate]['weights'][idx_r2])
            r2s.append(best_r2)

            best_r2_prob = bestvaluesfn['r2-prob'](metrics_dict[predicate]['r2-prob'])
            idx_r2_prob = metrics_dict[predicate]['r2-prob'].index(best_r2_prob)
            weights_r2_prob.append(metrics_dict[predicate]['weights'][idx_r2_prob])
            r2s_prob.append(best_r2_prob)

            best_mse_prob = bestvaluesfn['mse-prob'](metrics_dict[predicate]['mse-prob'])
            idx_mse_prob = metrics_dict[predicate]['mse-prob'].index(best_mse_prob)
            weights_mse_prob.append(metrics_dict[predicate]['weights'][idx_mse_prob])
            context_mse_prob.append(best_mse_prob)

            best_mrr = bestvaluesfn['mrr'](metrics_dict[predicate]['mrr'])
            idx_mrr = metrics_dict[predicate]['mrr'].index(best_mrr)
            weights_mrr.append(metrics_dict[predicate]['weights'][idx_mrr])
            mrrs.append(best_mrr)

            ts_pos = bestvaluesfn['train-set'](metrics_dict[predicate]['train-set'])
            train_sets.append(ts_pos)

            best_n_rules = bestvaluesfn['n-rules'](metrics_dict[predicate]['n-rules'])
            idx_n_rules = metrics_dict[predicate]['n-rules'].index(best_n_rules)
            weights_n_rules.append(metrics_dict[predicate]['weights'][idx_n_rules])
            n_rules.append(best_n_rules)


        if len(context_mse) > 0:
            metrics_dict['aggregation'] = {}
            metrics_dict['aggregation']['accuracy'] = np.average(context_accuracies, weights=weights_accuracy)
            metrics_dict['aggregation']['black-box-accuracy'] = np.average(blackbox_accuracies, weights=weights_bb_accuracy)
            metrics_dict['aggregation']['auc-roc'] = np.average(auc_rocs, weights=weights_auc_roc)
            metrics_dict['aggregation']['mse'] = np.average(context_mse, weights=weights_mse)
            metrics_dict['aggregation']['mse-stdev'] = np.std(context_mse)
            metrics_dict['aggregation']['mse-stdev'] = np.min(context_mse)
            metrics_dict['aggregation']['n_preds'] = n_preds
            metrics_dict['aggregation']['r2'] = np.average(r2s, weights=weights_r2)
            metrics_dict['aggregation']['r2-stdev'] = np.std(r2s)
            metrics_dict['aggregation']['r2-max'] = np.max(r2s)
            metrics_dict['aggregation']['r2-prob'] = np.average(r2s_prob, weights=weights_r2_prob)
            metrics_dict['aggregation']['r2-prob-stdev'] = np.std(r2s_prob)
            metrics_dict['aggregation']['r2-prob-max'] = np.max(r2s_prob)
            metrics_dict['aggregation']['mse-prob'] = np.average(context_mse_prob, weights=weights_mse_prob)
            metrics_dict['aggregation']['mse-prob-stdev'] = np.std(context_mse_prob)
            metrics_dict['aggregation']['mse-prob-stdev'] = np.min(context_mse_prob)
            metrics_dict['aggregation']['mrr'] = np.average(mrrs, weights=weights_mrr)
            metrics_dict['aggregation']['n_preds'] = len(metrics_dict.keys())
            metrics_dict['aggregation']['train-set'] = np.average(train_sets)
            metrics_dict['aggregation']['n-rules'] = np.average(n_rules, weights=weights_n_rules)


            total_coverage = 0
            coverage = 0
            for predicate in dataset_stats:
                total_coverage += dataset_stats[predicate]
                if predicate in metrics_dict:
                    coverage += dataset_stats[predicate]

            metrics_dict['aggregation']['coverage'] = coverage / total_coverage

def compute_histograms_for_all_files(files_dict) :
    for extype, extypedict in files_dict.items() :
        for dataset, datasetdict in extypedict.items() :
            for blackbox, blackboxdict in datasetdict.items() :
                for scope, scopedict in blackboxdict.items() :
                    for rulestype, rulestypedict in scopedict.items() :
                        (file_list, metrics_dict) = rulestypedict
                        for filename in file_list :
                            calculate_histogram_data_for_file(filename, extype, scope, metrics_dict)


def header(metrics=['accuracy', 'log-loss', 'auc-roc']) :
    output = '\\begin{table*}\n\\begin{tabular}{l'
    for i in range(6*len(metrics)) :
        output += 'C{0.75cm}'
    output += '}\n'

    headers_metric = []
    output += '&'
    for metric in metrics :
        headers_metric.append('\multicolumn{4}{c}{' + metric + '}')
    output += '&'.join(headers_metric) + ' \\\\ \\cmidrule[.1em]{2-13} \n'

    output += '&'
    for id, metric in enumerate(metrics) :
        headers_rule_type = ['\multicolumn{2}{c}{Unbounded}', '\multicolumn{2}{c}{Bounded}']
        output += '&'.join(headers_rule_type)
        if id < len(metrics) - 1 :
            output += '&'
    output += ' \\\\ \\cmidrule[.05em]{2-13} \n'

    output += '&'
    for id, metric in enumerate(metrics):
        locality = ['L', 'G', 'L', 'G']
        output += '&'.join(locality)
        if id < len(metrics) - 1 :
            output += '&'
    output += ' \\\\ \\toprule'

    return output

def footer() :
    return '\end{tabular}\n\end{table*}'


def output_tables(files_dict, dataset='fb15k', explanation_type='binary', metrics=['accuracy', 'log-loss', 'auc-roc']) :
    dimensions = ['pca-full-no-constant-rules-individual',
                  'pca-full-no-constant-rules-batch', 'pca-full-rules-individual', 'pca-full-rules-batch']
    print(header(metrics=metrics))

    #print(files_dict[explanation_type].keys())
    for method in files_dict[explanation_type][dataset] :
        output_dict = {}

        for locality in files_dict[explanation_type][dataset][method] :
            for rule_type in files_dict[explanation_type][dataset][method][locality] :
                (files_list, dict_list) = files_dict[explanation_type][dataset][method][locality][rule_type]

                for metric in metrics:
                    if metric not in output_dict:
                        output_dict[metric] = {}

                for metric in metrics :
                    if 'aggregation' in dict_list :
                        output_dict[metric][rule_type + '-' + locality] = dict_list['aggregation'][metric]
                    else :
                        output_dict[metric][rule_type + '-' + locality] = float('-inf')

        output = method + '&'
        for id, metric in enumerate(metrics):
            values = []
            for x in dimensions :
                if x in output_dict[metric] :
                    values.append('{:.2f}'.format(output_dict[metric][x]))
                else :
                    values.append('--')

            output += '&'.join(values)
            if id < len(metrics) - 1:
                output += '&'
        output += '\\\\ \n'
        sys.stdout.write(output)

    print(footer())

def output_histograms(files_dict, dataset='fb15k', explanation_type='binary', metric='accuracy', **args) :
    for method in files_dict[explanation_type][dataset] :

        for locality in files_dict[explanation_type][dataset][method] :
            for rule_type in files_dict[explanation_type][dataset][method][locality] :
                (files_list, dict_list) = files_dict[explanation_type][dataset][method][locality][rule_type]
                items = []
                data_in_curve = None
                if 'data' in args :
                    data_in_curve = []
                for predicate in dict_list :
                    if predicate == 'aggregation' :
                        continue
                    if metric in dict_list[predicate] :
                        values = dict_list[predicate]
                        if len(values) > 0 :
                            best_value = bestvaluesfn[metric](dict_list[predicate][metric])
                            items.append((predicate, best_value))
                            if 'data' in args :
                                if isinstance(args['data'], dict) :
                                    if predicate in args['data'] :
                                        data_in_curve.append((predicate, args['data'][predicate], best_value))
                                elif isinstance(args['data'], str) :
                                    metric2 = args['data']
                                    curve_value = bestvaluesfn[metric2](dict_list[predicate][metric2])
                                    data_in_curve.append((predicate, curve_value, best_value))
                if len(items) > 0 :
                    sortedDict = sorted(items, key=lambda x: x[1],
                                        reverse=(bestvaluesfn[metric] == np.max))
                    if data_in_curve is not None :
                        sortedDict2 = sorted(data_in_curve, key=lambda x: x[2],
                                            reverse=(bestvaluesfn[metric] == np.max))
                    fig1, ax1 = plt.subplots(figsize=(10,8))
                    ax1.bar([i for i in range(1, len(sortedDict) + 1)], [x[1] for x in sortedDict], color='blue')
                    ax1.set_xlabel('Predicate')
                    ax1.xaxis.label.set_size(20)
                    ax1.set_ylabel(metric.upper())
                    ax1.yaxis.label.set_color('blue')
                    ax1.yaxis.label.set_size(20)
                    ax1.spines['left'].set_color('blue')
                    ax1.tick_params(axis='y', colors='blue')
                    for tick in ax1.xaxis.get_major_ticks():
                        tick.label.set_fontsize(18)
                    for tick in ax1.yaxis.get_major_ticks():
                        tick.label.set_fontsize(18)
                    if data_in_curve is not None :
                        ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
                        ax2.plot([i for i in range(1, len(sortedDict) + 1)], [x[1] for x in sortedDict2], color='red', linewidth=1.0)
                        if isinstance(args['data_label'], str) :
                            ax2.set_ylabel(args['data_label'].capitalize())
                            ax2.yaxis.label.set_color('red')
                            ax2.yaxis.label.set_size(20)
                            ax2.tick_params(labelsize=18)
                            #for tick in ax2.yaxis.get_minor_ticks():
                            #    tick.label.set_fontsize(18)
                            ax2.spines['right'].set_color('red')
                            ax2.tick_params(axis='y', colors='red')

                    if sortedDict2 is not None :
                        print(metric, locality, rule_type, explanation_type, method, 'best', sortedDict2[:5], 'worst',
                              sortedDict2[-5:], 'pearson=', pearsonr([x[2] for x in sortedDict2], [x[1] for x in sortedDict2]),
                              'spearman=', spearmanr([x[2] for x in sortedDict2], [x[1] for x in sortedDict2]))
                    else :
                        print(metric, locality, rule_type, explanation_type, method, 'best', sortedDict[:5],
                              'worst', sortedDict[-5:])

                    if maxvalues[metric] != float('inf') :
                        ax1.set_ylim(top=maxvalues[metric])
                    if 'data_label' in args :
                        suffix = args['data_label']
                    else :
                        suffix = 'facts'

                    fig1.savefig('histogram_' + method + '_' + dataset + '_' + locality + '_' + rule_type + '_'
                                 + metric + '_' + suffix + '.png')

def get_stats(file_stats) :
    stats = {}
    with open(file_stats, 'r') as infile :    
        somefile_csv = csv.reader(infile, delimiter='\t')                
        for row in somefile_csv :
            if len(row) >= 2 :
                stats[row[0]] = int(row[1])

    return stats

def get_functionality_scores(train_file, inverse=False) :
    subs = {}
    facts = {}
    with open(train_file, 'r') as infile :
        somefile_csv = csv.reader(infile, delimiter='\t')
        for row in somefile_csv :
            if len(row) >= 2 :
                predicate = row[1]
                if predicate not in subs :
                    subs[predicate] = set()
                    facts[predicate] = 0

                subs[predicate].add(row[0] if not inverse else row[2])
                facts[predicate] += 1

    funs = {}
    for predicate in facts :
        funs[predicate] = len(subs[predicate]) / facts[predicate]

    return funs

if __name__ == '__main__' :
    ## Organize the files
    test_stats = get_stats(sys.argv[2])
    files_dict = get_files_dictionary(sys.argv[1])
    ## Calculate the corresponding metrics and update the dictionary
    calculate_metrics_for_all_files(files_dict, test_stats)
    ## Format the tables
    dataset = sys.argv[1].rstrip('/').split('/')[-1]
    print('Dataset', dataset)
    #output_tables(files_dict, dataset=dataset, explanation_type='binary', metrics=['accuracy', 'log-loss', 'auc-roc', 'mrr', 'coverage'])
    #output_tables(files_dict, dataset=dataset, explanation_type='linear', metrics=['mse-prob', 'r2-prob', 'mrr', 'coverage'])
    output_tables(files_dict, dataset=dataset, explanation_type='linearm', metrics=['auc-roc', 'mrr', 'coverage'])
    #output_tables(files_dict, dataset=dataset, explanation_type='linearm', metrics=['auc-roc', 'train-set'])
    #output_tables(files_dict, dataset=dataset, explanation_type='linearm', metrics=['accuracy', 'total-rules'])

    #compute_histograms_for_all_files(files_dict)
    #output_histograms(files_dict, dataset=dataset, explanation_type='linearm', metric='accuracy')
    #output_histograms(files_dict, dataset=dataset, explanation_type='binary', metric='log-loss')
    data_in_curves = test_stats
    data_label = 'facts'
    if len(sys.argv) > 3 :
        if sys.argv[3] == 'functionality' or sys.argv[3] == 'inv-functionality' :
            data_in_curves = get_functionality_scores(os.path.dirname(sys.argv[2]) + "/train.tsv", inverse=('inv-' in sys.argv[3]))
        else :
            data_in_curves = sys.argv[3]
        if sys.argv[3] == 'n-rules' :
            data_label = '# of rules'
    #output_histograms(files_dict, dataset=dataset, explanation_type='linearm', metric='auc-roc',
    #                  additional_metric='size', data=data_in_curves, data_label=sys.argv[3])
    output_histograms(files_dict, dataset=dataset, explanation_type='linearm', metric='mrr',
                      additional_metric='size', data=data_in_curves, data_label=data_label)

    #output_histograms(files_dict, dataset=dataset, explanation_type='linearm', metric='bb-accuracy',
    #                  additional_metric='size', data=data_in_curves, data_label=sys.argv[3])