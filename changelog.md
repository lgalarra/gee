# Changelog

## 01/03/2021

- remove "rules" explanation
- implement logging
- python `venv` management & `requirements.txt`
- use `docopt` to manage scripts arguments
- add .gitignore
- update readme
- `skge` fork and ease installation via `pip install`
- a LOT of automatic syntax formatting
- provide a LOT of type hints
- small performance improvement for collecting subject & object neighbors
- create `utils` module to group utility & miscellaneous functions split across project
