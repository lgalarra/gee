# Explaining Embedding-based Models for Link Prediction in Knowledge Graphs

This repository contains the code and experimental data from the paper [*Explaining Embedding-based Models for Link
Prediction in Knowledge Graphs*](https://openreview.net/forum?id=1nEMe9QGuPS).

## Package Dependencies

You'll need java JRE correctly setup in your path (tested with openjdk 11.0.10).

For the python part, we recommend creating a [virtual environment](https://docs.python.org/3/library/venv.html) for this
project.

```shell
cd gee
python3 -m venv venv
source venv/bin/activate
```

Here's the content of the `requirements.txt` file that contains almost all the requirements

```
cycler==0.10.0
docopt==0.6.2
joblib==1.0.1
kiwisolver==1.3.1
matplotlib==3.3.4
nose==1.3.7
numpy==1.20.1
Pillow==8.1.0
pyparsing==2.4.7
python-dateutil==2.8.1
scikit-learn==0.24.1
scipy==1.6.0
six==1.15.0
threadpoolctl==2.1.0
```

You can install those with the following command :

```shell
pip install -r requirements.txt
```

You'll still need to install the `scikit-kge` package from [github](https://github.com/Tazoeur/scikit-kge)

```shell
mkdir venv/custom_packages/
cd venv/custom_packages
git clone git@github.com:Tazoeur/scikit-kge.git skge
cd skge
pip install .
```

## Datasets

The experimental datasets are available under resources/data. There is a directory per dataset. The data is divided into
train and test sets stored in the files train.tsv and test.tsv respectively. By default the training routine uses the
file train.tsv, whereas the explainer uses test.tsv to learn and evaluate the explanations.

## Experimental Data

## Training models

To train models you can use the `train.py` script for which you can find the following documentation:

```
Gee Train

Usage:
    train <dataset> <method> <latent_factors> <counter_examples> [options] [--verbose | -v | -vv | -vvv]
    train -h | --help

Arguments:
    <dataset>           Any of the directory names under resources/data.
                        The program automatically reads the file train.tsv
    <method>            hole|rescal|transe
    <latent_factors>    The dimension of the embeddings space (positive integer)
    <counter_examples>  The number of counter-examples generated per each example in train.tsv
                        This value can be either an integer greater than 0, or a real number between 0 and 1.
                        E.g if this value is 0.2=1/5, then a counter-example will be generated for every 5 facts in
                        train.tsv

Options:
    -f --force-train    Replace existing trained model if any.
    --log-file=<path>   If provided, the logs will (also) be written in this file.
    --quiet -q          Do not print anything in STDOUT (to keep logs, please use the --log-file option)
                        Please note that Critical/Error levels will still be outputted to STDERR.
    --verbose -v        Display more or less information (0: Critical/Error, -v: Warning, -vv: Info, -vvv: Debug).
    -h --help           Show this screen.
```

An example could be:

`$ python3 train.py fb15k-237-wcp transe 50 1`

This generates a binary file with the following naming convention (so make sure the directory exists beforing launching
the training program):

`resources/models/DATASET_METHOD_N-LATENT-FACTORS_COUNTEREXAMPLES-PER-EXAMPLE.pkl`,

as well as a text file
named `resources/models/DATASET_METHOD_N-LATENT-FACTORS_COUNTEREXAMPLES-PER-EXAMPLE_counterexamples.tsv` with the
counterexamples generated for training. The terms in the triples are assigned unique integer identifiers.

Training takes time, so you can also use
the [already trained models](https://drive.google.com/file/d/1T4nhX_0iol2mtbj-gv35-k_qVn-WgIEB/view?usp=sharing).

## Computing Explanations

To compute explanations you can run the `explain.py` script for which you can find the following documentation:

```
Gee Explain

Usage:
    explain list [--verbose|-v|-vv|-vvv]
    explain <dataset> <method> <latent_factors> <counter_examples> <rule_language> [options] [--verbose|-v|-vv|-vvv]
    explain -h | --help

Arguments:
    list                List the model(s) available for explanation computations.
    <dataset>           Any of the directory names under resources/data. The program automatically reads the file
                        train.tsv.
    <method>            hole|rescal|transe
    <latent_factors>    The dimension of the embeddings space (positive integer).
    <counter_examples>  The number of counter-examples generated per each example in train.tsv.
                        This value can be either an integer greater than 0, or a real number between 0 and 1.
                        E.g if this value is 0.2=1/5, then a counter-example will be generated for every 5 facts in
                        train.tsv.
    <rule_language>     pca-full-no-constant-rules|pca-full-rules
                        The first option mines rule with no constants.
                        The second allows for instantiated atoms in rules.

Options:
    -t=<threshold> --threshold=<threshold>  Coma-separated list of threshold values used to decide whether a black-box
                                            prediction is interpreted as a positive or negative prediction about the
                                            truth of a fact [default: 0.5,0.75,0.85,0.95].
    -l --local                              For local explanation computations.
    -g --global                             For global explanation computations.
    -r --recursive-rules                    If present, the body of the rules can contain the target predicate, allowing
                                            for rules such as marriedTo(x, y) -> __marriedTo(y, x).
                                            (The __ is a prefix that allows us to create a surrogate predicate that
                                            represents the verdicts of the black-box link predictor).
    --log-file=<path>                       If provided, the logs will (also) be written in this file.
    --quiet -q                              Do not print anything in STDOUT (to keep logs, please use --log-file)
                                            Please note that Critical/Error levels will still be outputted to STDERR.
    -v --verbose                            Display more or less information
                                            (0: Critical/Error, -v: Warning, -vv: Info, -vvv: Debug).
    -h --help                               Show this screen.
```

An example is:

`$ python3 explain.py nell186 hole 50 1 pca-full-no-constant-rules True batch 0.5`

This will generate a text file `experiments/nell186/transe_50_1_pca-full-no-constant-rules_0.5_batch_linearm.tsv`. This
file contains a line per predicate (global explanations) or fact (local explanations) with the corresponding
explanations and the different performance scores (AUC-ROC, MRR, etc). Learning explanations can take time too, so we
also provide the [experimental data](https://drive.google.com/file/d/1cjkkp5lWsKGAXWFDqkEeCa1rqObgKayu/view?usp=sharing)
produced for the paper. The package contains a README file that provides some hints on how to interpret the data in the
files.

### Outputting results

To get the results of the experimental files as LateX tables and histograms you can launch:

`$ python3 compute_metrics.py DATASET TRAIN_DATA_AGGREGATES SECOND_CRITERIA`

An example is :

`$ python3 analysis/compute_metrics.py ../experiments/fb15k-237-wcp/ ../resources/data/fb15k-237-wcp/stats_train.tsv n-rules`

By default the program builds LateX tables that are output to stdout and report the *AUC-ROC*, the *MRR* and the *
coverage* metrics for all rule languages and locality settings (as defined in
the [paper](https://openreview.net/pdf?id=1nEMe9QGuPS)). The histograms plot those metrics for all predicates explained
in the experimental files (sorted in descending order of MRR). The *SECOND_CRITERIA* argument defines an additional
y-axis to constrast the MRR with other scores such as: n-rules (# of rules), functionality, inv-functionality, facts (#
of facts per predicate). Figure 1 in the paper was obtained via the example command. The charts are stored under the
directory analysis/. The metrics output in the tables and charts can be easily changed by editing the code.