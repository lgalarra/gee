files = ['test.tsv', 'train.tsv']

with open('fb15k_cartesian.txt') as rfobj :
	lines = rfobj.readlines()
	filtered_relations = set([l.rstrip('\n') for l in lines])
	for f in files :
		with open(f, 'r') as fobj :
			with open(f + '_filtered', 'w') as fobjout :
				for l in fobj :
					triple = l.split('\t')
					if triple[1] not in filtered_relations :
						fobjout.write(l)	
