#!/usr/bin/python3

"""
Gee Train

Usage:
    train <dataset> <method> <latent_factors> <counter_examples> [options] [--verbose | -v | -vv | -vvv]
    train -h | --help

Arguments:
    <dataset>           Any of the directory names under resources/data.
                        The program automatically reads the file train.tsv
    <method>            hole|rescal|transe
    <latent_factors>    The dimension of the embeddings space (positive integer)
    <counter_examples>  The number of counter-examples generated per each example in train.tsv
                        This value can be either an integer greater than 0, or a real number between 0 and 1.
                        E.g if this value is 0.2=1/5, then a counter-example will be generated for every 5 facts in
                        train.tsv

Options:
    -f --force-train    Replace existing trained model if any.
    --log-file=<path>   If provided, the logs will (also) be written in this file.
    --quiet -q          Do not print anything in STDOUT (to keep logs, please use the --log-file option)
                        Please note that Critical/Error levels will still be outputted to STDERR.
    --verbose -v        Display more or less information (0: Critical/Error, -v: Warning, -vv: Info, -vvv: Debug).
    -h --help           Show this screen.
"""

import os

from docopt import docopt

from data.kb import get_training_kb
from explainers.factory import get_model
from utils.logger import configure_logger

if __name__ == '__main__':
    arguments = docopt(__doc__)
    logger = configure_logger(arguments['--verbose'], arguments['--log-file'], arguments['--quiet'])
    kb = get_training_kb(os.path.abspath(f'resources/data/{arguments["<dataset>"]}'))

    n_counter_ex = arguments['<counter_examples>']
    n_counter_ex = int(n_counter_ex) if str.isdigit(n_counter_ex) else float(n_counter_ex)

    kb.fit()
    get_model(kb, arguments['<method>'], int(arguments['<latent_factors>']), n_counter_ex,
              force_training=arguments['--force-train'])
    logger.info(f"Finished training of {arguments['<method>']} on {arguments['<dataset>']}")
