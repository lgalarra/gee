import csv
import os
import tempfile
from pathlib import Path

import numpy as np
from scipy.special import expit
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.metrics import mean_squared_error, r2_score, accuracy_score, log_loss, roc_auc_score

from data.kb import TRIPLE, MODEL_ANSWER, TRUTH_VALUE
from explainers.metrics import expit_mean_squared_error, expit_r2
from utils.logger import get_logger
from utils.misc import get_random_string

logger = get_logger()

complementary_metrics = {expit_mean_squared_error: expit_r2, mean_squared_error: r2_score}


class Explanation:
    def __init__(self, kb, global_context, local_context_positive, local_context_negative,
                 target_instance=None):
        self.kb = kb
        self.global_context = global_context
        self.local_context_positive = local_context_positive
        self.local_context_negative = local_context_negative
        self.local_context = None if local_context_positive is None and local_context_negative is None \
            else self.local_context_positive + self.local_context_negative
        # When we want to generate explanations for a single prediction
        self.target_instance = target_instance

    def scores(self):
        return None


class EmptyExplanation(Explanation):
    def __init__(self, kb=None, global_context=None, local_context_positive=None, local_context_negative=None):
        super().__init__(kb, global_context, local_context_positive, local_context_negative)

    def scores(self):
        super(EmptyExplanation, self).scores()


class RuleBasedExplanation(Explanation):
    from explainers.discretization_functions import greater_than_median

    def __init__(self, kb, global_context, local_context_positive, local_context_negative,
                 target_instance, rules, threshold_fn=greater_than_median):
        super(RuleBasedExplanation, self).__init__(kb, global_context, local_context_positive, local_context_negative,
                                                   target_instance=target_instance)
        self.rules = rules
        self.threshold_fn = threshold_fn
        self.model_answers = []
        for instance in self.local_context:
            self.model_answers.append(instance[MODEL_ANSWER])

        self._context_ranking = sorted(self.local_context,
                                       key=lambda x: x[MODEL_ANSWER], reverse=True)
        self._local_positive_context_ranking = sorted(self.local_context_positive,
                                                      key=lambda x: x[MODEL_ANSWER], reverse=True)

    def get_rules(self):
        return self.rules

    def output(self):
        return str(self.get_rules())

    def _parse_answers(self, text):
        veredicts = []
        lines = text.split('\n')
        for i in range(4, len(lines)):
            if len(lines[i]) > 0:
                veredicts.append([float(v) for v in lines[i].split('\t')])

        return veredicts

    def rank(self, instance, ranking):
        for index, triple in enumerate(ranking):
            if triple[TRIPLE] == instance[TRIPLE]:
                return index + 1

        return len(ranking)

    def kendall_tau(self, rules_ranking):
        kendall = 0
        n = len(self.local_context_positive)

        for i, instance_i in enumerate(self._local_positive_context_ranking):
            rank_i = self.rank(instance_i, rules_ranking)
            for j in range(i + 1, len(self._local_positive_context_ranking)):
                instance_j = self._local_positive_context_ranking[j]
                rank_j = self.rank(instance_j, rules_ranking)
                if (rank_i > rank_j and i > j) or (rank_i < rank_j and i < j):
                    kendall += 1

        if n > 1:
            return 2 * kendall / (n * (n - 1))
        else:
            return 1.0

    def mean_ranks(self, rules_ranking):
        ## MR: for the single case, just the index of the target instance in the sorted ranking
        ## MRR: consider every positive example as a query and compute according to
        ## https://en.wikipedia.org/wiki/Mean_reciprocal_rank
        rankings = []
        mean_rank_target = -1.0
        mean_rank_context = 0.0
        mean_rec_rank_context = 0.0
        if self.target_instance is not None:
            mean_rank_target = self.rank(self.target_instance, rules_ranking)

        n = len(self.local_context_positive)

        if n > 0:
            for index, positive_instance in enumerate(self.local_context_positive):
                instance_rank = self.rank(positive_instance, rules_ranking)
                rankings.append(instance_rank)
                mean_rank_context += instance_rank
                mean_rec_rank_context += 1. / instance_rank

            return mean_rank_target, mean_rank_context / n, mean_rec_rank_context / n
        else:
            return mean_rank_target, mean_rank_context, mean_rec_rank_context

    def rules_answers_on_context(self, learning_context):
        """
         It returns (i) the aggregated score of the explanation in the set of instances in the context,
        and (ii) the score of the instance at the center of the vicinity defined by that context
        """
        with tempfile.NamedTemporaryFile(mode='w+') as tkbfile:
            kbcsvfile = csv.writer(tkbfile, delimiter='\t')
            for triple in self.global_context:
                text_triple = self.kb.convert_to_textual_triple(triple)
                kbcsvfile.writerow(text_triple)

            tkbfile.seek(0)

            with tempfile.NamedTemporaryFile(mode='w+') as tfile:
                csvfile = csv.writer(tfile, delimiter='\t')
                for triple in learning_context:
                    csvfile.writerow(triple[TRIPLE])

                tfile.seek(0)

                with tempfile.NamedTemporaryFile(mode='w+') as trulesfile:
                    csvrulesfile = csv.writer(trulesfile, delimiter='\t')
                    for rule in self.rules:
                        csvrulesfile.writerow(rule.to_tuple())

                    trulesfile.seek(0)

                    # Evaluate the triples against the rules
                    eval_output = 'eval-stdout' + get_random_string(8)
                    cmd_line = 'java -jar lib/rule-eval.jar ' + tkbfile.name + ' ' + tfile.name + ' ' \
                               + trulesfile.name \
                               + ' false > ' + eval_output
                    logger.info('Executing java')
                    logger.debug(cmd_line)

                    os.system(cmd_line)
                    contents = Path(eval_output).read_text()
                    veredicts = self._parse_answers(contents)
                    os.remove(eval_output)

                    return veredicts

    def scores(self):
        veredicts = self.rules_answers_on_context(self.local_context)
        binary_answers = []
        binary_model_answers = []
        raw_answers = []
        probabilities_rules = []
        n_positive_rules = []
        n_rules = []
        positive_triples_and_answers = []
        context_triples_and_answers = []
        ground_truth = []

        target_instance_answer_index = -1
        target_instance_acc_score = -1.0

        for index, veredict in enumerate(veredicts):
            veredict_array = np.array(veredict)
            raw_answer = np.sum(veredict_array)
            raw_answers.append(raw_answer)
            probabilities_rules.append(expit(raw_answer))

            binary_answer = self.threshold_fn(raw_answer)
            binary_answers.append(binary_answer)
            n_rules.append(np.count_nonzero(veredict_array))
            n_positive_rules.append(np.count_nonzero(veredict_array > 0))

            context_triples_and_answers.append((self.local_context[index], raw_answer))
            binary_model_answers.append(self.threshold_fn(self.local_context[index][MODEL_ANSWER]))
            ground_truth.append(1 if self.local_context[index][TRUTH_VALUE] > 0 else 0)

            ## The first elements correspond to the positive examples
            if index < len(self.local_context_positive):
                positive_triples_and_answers.append((self.local_context_positive[index], raw_answer))

            ## I do not like the idea of asking for this at every iteration
            ## but I have not found so far a more efficient way :/
            if self.target_instance is not None:
                if self.local_context[index][TRIPLE] == self.target_instance[TRIPLE]:
                    target_instance_answer_index = index

        avg_model_answer = np.mean(binary_model_answers)
        acc_weights = None
        ## If we witness imbalance
        if np.max(binary_model_answers) != np.min(binary_model_answers):
            acc_weights = [1.0 - avg_model_answer if x == 1 else avg_model_answer for x in binary_model_answers]

        if target_instance_answer_index > -1:
            target_instance_acc_score = accuracy_score([binary_model_answers[target_instance_answer_index], ],
                                                       [binary_answers[target_instance_answer_index], ])

        context_acc_score = accuracy_score(binary_model_answers, binary_answers, sample_weight=acc_weights)
        context_log_loss_score = log_loss(binary_model_answers, probabilities_rules)
        context_roc_auc = roc_auc_score(binary_model_answers, raw_answers)

        avg_ground_truth = np.mean(ground_truth)
        gt_weights = None
        if np.max(ground_truth) != np.min(ground_truth):
            gt_weights = [1.0 - avg_ground_truth if x == 1 else avg_ground_truth for x in ground_truth]
        bb_acc_score = accuracy_score(ground_truth, binary_model_answers, sample_weight=gt_weights)

        rules_real_acc_score = accuracy_score(ground_truth, binary_answers, sample_weight=gt_weights)

        ## Time to calculate the ranks
        context_triples_and_answers.sort(key=lambda x: x[0][2], reverse=True)
        rules_ranking = [x[0] for x in context_triples_and_answers]
        mean_rank_target, mean_rank_context, mean_reciprocal_rank = self.mean_ranks(rules_ranking)

        ## For kendall we need the answers of the positive instances
        positive_triples_and_answers.sort(key=lambda x: x[0][2], reverse=True)
        rules_ranking_for_positive_instances = [x[0] for x in positive_triples_and_answers]

        kendall = self.kendall_tau(rules_ranking_for_positive_instances)

        result = {'context-acc-score': context_acc_score, 'target-instance-acc-score': target_instance_acc_score,
                  'mean-rank-target': mean_rank_target, 'context-log-loss': context_log_loss_score,
                  'context-auc-roc': context_roc_auc,
                  'mean-rank-context': mean_rank_context, 'mean-reciprocal-rank': mean_reciprocal_rank,
                  'kendall-rank-correlation': kendall, 'rules-answer-avg': np.mean(binary_answers),
                  'model-answer-avg': avg_model_answer, 'positive-rules-per-fact': np.mean(n_positive_rules),
                  'rules-per-fact': np.mean(n_rules), 'bb-acc-score': bb_acc_score,
                  'rules-real-acc-score': rules_real_acc_score}
        return result


class LinearRuleBasedExplanation(RuleBasedExplanation):
    def __init__(self, kb, global_context, local_context_positive, local_context_negative,
                 target_instance, rules, rule_training_context, threshold_fn=None):
        super(LinearRuleBasedExplanation, self).__init__(kb, global_context, local_context_positive,
                                                         local_context_negative,
                                                         target_instance, rules,
                                                         threshold_fn=threshold_fn)

        self.rule_training_context = rule_training_context
        self.regressor1 = LinearRegression()
        self.regressor2 = LinearRegression()
        self.regressor3 = LogisticRegression()

    def output(self):
        return str(self.get_rules()) + ' --- [' + ';'.join(
            [str(x) for x in self.regressor1.coef_.flatten()]) + '], ' + str(self.regressor1.intercept_)

    def scores(self):
        veredicts = self.rules_answers_on_context(self.rule_training_context)
        answers = []
        answers_prob = []
        binary_answers = []

        ## Get the model answers
        for instance in self.rule_training_context:
            answers.append(instance[MODEL_ANSWER])
            answers_prob.append(expit(instance[MODEL_ANSWER]))
            binary_answers.append(self.threshold_fn(instance[MODEL_ANSWER]))

        try:
            if len(veredicts) == 1:
                if len(veredicts[0]) == 1:
                    self.regressor1.fit(np.reshape(veredicts, (1, -1)), answers)
                    self.regressor2.fit(np.reshape(veredicts, (1, -1)), answers_prob)
                    self.regressor3.fit(np.reshape(veredicts, (1, -1)), binary_answers)
                else:
                    self.regressor1.fit(np.reshape(veredicts, (-1, 1)), answers)
                    self.regressor2.fit(np.reshape(veredicts, (-1, 1)), answers_prob)
                    self.regressor3.fit(np.reshape(veredicts, (-1, 1)), binary_answers)
            else:
                self.regressor1.fit(veredicts, np.ravel(answers, order='C'))
                self.regressor2.fit(veredicts, np.ravel(answers_prob, order='C'))
                self.regressor3.fit(veredicts, np.ravel(binary_answers, order='C'))
        except:
            return None

        test_answers = []
        test_answers_prob = []
        test_ground_truth = []
        test_binary_answers = []
        for idx, instance in enumerate(self.local_context):
            test_answers.append(instance[MODEL_ANSWER])
            test_answers_prob.append(expit(instance[MODEL_ANSWER]))
            test_ground_truth.append(1 if self.local_context[idx][TRUTH_VALUE] > 0 else 0)
            test_binary_answers.append(self.threshold_fn(instance[MODEL_ANSWER]))

        test_veredicts = self.rules_answers_on_context(self.local_context)

        if len(test_veredicts) == 1:
            if len(test_veredicts[0]) == 1:
                test_predictions = self.regressor1.predict(np.reshape(test_veredicts, (1, -1)))
                test_predictions_prob = self.regressor2.predict(np.reshape(test_veredicts, (1, -1)))
                test_binary_predictions = self.regressor3.predict(np.reshape(test_veredicts, (1, -1)))
            else:
                test_predictions = self.regressor1.predict(np.reshape(test_veredicts, (-1, 1)))
                test_predictions_prob = self.regressor2.predict(np.reshape(test_veredicts, (-1, 1)))
                test_binary_predictions = self.regressor3.predict(np.reshape(test_veredicts, (-1, 1)))
        else:
            test_predictions = self.regressor1.predict(test_veredicts)
            test_predictions_prob = self.regressor2.predict(test_veredicts)
            test_binary_predictions = self.regressor3.predict(test_veredicts)

        avg_ground_truth = np.mean(test_ground_truth)
        gt_weights = None
        if np.max(test_ground_truth) != np.min(test_ground_truth):
            gt_weights = [1.0 - avg_ground_truth if x == 1 else avg_ground_truth for x in test_ground_truth]
        linear_real_acc_score = accuracy_score(test_ground_truth, test_binary_answers, sample_weight=gt_weights)

        context_score = mean_squared_error(test_answers, test_predictions)
        context_r2 = r2_score(test_answers, test_predictions)

        context_score_prob = mean_squared_error(test_answers_prob, test_predictions_prob)
        context_r2_prob = r2_score(test_answers_prob, test_predictions_prob)

        context_acc_score = accuracy_score(test_binary_answers, test_binary_predictions)
        context_log_loss_score = log_loss(test_binary_answers, test_predictions_prob)
        context_roc_auc = roc_auc_score(test_binary_answers, test_predictions_prob)

        context_triples_and_answers = []
        positive_triples_and_answers = []
        target_instance_mse_score = -1.0
        target_instance_mse_score_prob = -1.0
        n_rules = []
        for index, veredict in enumerate(test_veredicts):
            answer = test_predictions[index]
            answer_prob = test_predictions_prob[index]
            context_triples_and_answers.append((self.local_context[index], answer))
            n_rules.append(np.count_nonzero(np.array(veredict)))

            if self.target_instance is not None:
                if self.local_context[index][TRIPLE] == self.target_instance[TRIPLE]:
                    target_instance_mse_score = mean_squared_error([self.target_instance[MODEL_ANSWER], ],
                                                                   [answer, ])
                    target_instance_mse_score_prob = mean_squared_error([expit(self.target_instance[MODEL_ANSWER]), ],
                                                                        [answer_prob, ])

            ## The first elements correspond to the positive examples
            if index < len(self.local_context_positive):
                positive_triples_and_answers.append((self.local_context_positive[index], answer))

        ## Time to calculate the ranks
        context_triples_and_answers.sort(key=lambda x: x[1], reverse=True)
        rules_ranking = [x[0] for x in context_triples_and_answers]
        mean_rank_target, mean_rank_context, mean_reciprocal_rank = self.mean_ranks(rules_ranking)

        ## For kendall we need the answers of the positive instances
        positive_triples_and_answers.sort(key=lambda x: x[1], reverse=True)
        rules_ranking_for_positive_instances = [x[0] for x in positive_triples_and_answers]
        kendall = self.kendall_tau(rules_ranking_for_positive_instances)

        return {'context-acc-score': context_acc_score, 'context-mse-score': context_score,
                'target-instance-mse-score': target_instance_mse_score,
                'context-log-loss': context_log_loss_score, 'context-auc-roc': context_roc_auc,
                'context-r2': context_r2, 'context-mse-score-prob': context_score_prob,
                'target-instance-mse-score-prob': target_instance_mse_score_prob,
                'context-r2-prob': context_r2_prob,
                'rules-per-fact': np.average(n_rules), 'mean-rank-target': mean_rank_target,
                'mean-rank-context': mean_rank_context, 'mean-reciprocal-rank': mean_reciprocal_rank,
                'kendall-rank-correlation': kendall, 'linear-real-acc-score': linear_real_acc_score}
