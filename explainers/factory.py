import csv
import os
import pickle
import time
from typing import Union, TextIO

import numpy as np
from skge import HolE, TransE, RESCAL, StochasticTrainer, PairwiseStochasticTrainer

from data.kb import TRIPLE, KB, TRUTH_VALUE
from data.sample import ImprovedCachedLCWASampler
from utils.logger import get_logger
from utils.types import Fact, KGEModel

logger = get_logger()


def save_counter_examples(quads: set, file_obj: TextIO, delimiter: str = '\t'):
    csv_writer = csv.writer(file_obj, delimiter=delimiter)
    for quad in quads:
        csv_writer.writerow(tuple(quad[TRIPLE]))


def get_model_prefix(dataset_path: os.path, method: str, latent_factors: int, n_counter_examples: Union[float, int]):
    """
    Get an uniform name for a model, based on the different parameters
    """
    storage = 'resources/models'
    if not os.path.isdir(storage):
        logger.warning(f'Folder "{storage}" do not exist, creating it for ya.')
        os.makedirs(storage, exist_ok=True)

    dataset_parts = dataset_path.split('/')
    data_part = dataset_parts[-1]
    return os.path.join(os.path.abspath(storage), f'{data_part}_{method}_{latent_factors}_{n_counter_examples}')


def get_model(kb: KB, method: str = 'hole', latent_factors: int = 10, n_counter_examples: Union[float, int] = 1,
              force_training: bool = False) -> tuple[KGEModel, [(Fact, int)]]:
    """
    :param kb: knowledge base object (instance of class data.KB)
    :param method: hole, rescal, transe
    :param latent_factors: number of latent components used
    :param n_counter_examples: maximal number of counter-examples per positive example in the training set
    :param force_training: if True, it forces the function to ignore pre-trained models and retrain
    :return:
    :rtype: tuple[KGEModel, [(Fact, int)]]
    """
    if not force_training:
        # Look if something has already been trained
        model_name = get_model_prefix(kb.get_data_root(), method, latent_factors, n_counter_examples)
        model_path = model_name + '.pkl'
        counter_examples_path = model_name + '_counterexamples.tsv'
        if os.path.exists(model_path):
            logger.info(f'Getting already trained model from {model_path}')
            with open(model_path, 'rb') as model_file:
                model_obj = pickle.load(model_file)
            logger.debug(f'Pickle model loaded from {model_path}')
            return model_obj, _load_counter_examples(counter_examples_path)

    logger.info(f'Training {method} on {kb.dataset_name}.')
    return _get_and_train_model(kb, method, latent_factors, n_counter_examples)


def _load_counter_examples(filename: str, delimiter: str = '\t') -> [(Fact, int)]:
    """
    Load the counter examples stored in the file filename.
    The returned type is intended to be the same as KB.triples
    """
    logger.debug(f'Loading counter example from {filename}')
    counter_examples = []
    with open(filename, 'r') as file:
        csv_reader = csv.reader(file, delimiter=delimiter)
        for row in csv_reader:
            counter_examples.append((tuple([int(x) for x in row]), -1.0))

    return counter_examples


def _get_and_train_model(kb: KB, method: str = 'hole', latent_factors: int = 10,
                         n_counter_examples: Union[int, float] = 1) -> tuple[KGEModel, [(Fact, int)]]:
    """
    :param kb: The KB
    :param method: The link prediction method.
    :param latent_factors: The dimensions of the embedded space used by the method.
    :param n_counter_examples: The ratio of counter example by statement that needs to be produced.
    :return: The model and the KB tuples (with truth value)
    :rtype: tuple[KGEModel, [(Fact, int)]]
    """
    # Load knowledge graph
    # n = number of entities
    # m = number of relations
    # xs = list of tuples of the form ((id subject, id object, id predicate), 1)
    # id2entity = dictionary entity id -> string (valid for subjects and objects)
    # id2predicate = dictionary predicate id -> string
    n, m, xs_train, id2entity, id2predicate = kb.get_info()
    sz = (n, n, m)
    ys = np.ones(n)

    # extract the statements as a list of (subject, object, statement)
    logger.debug('Extracting valid statements from the KB.')
    xs_train_triples: [Fact] = [t[TRIPLE] for t in xs_train if t[TRUTH_VALUE] == 1]

    # instantiate a cached Local Closed Word Assumption sampler
    logger.debug('Instantiate a Sampler.')
    train_sampler = ImprovedCachedLCWASampler(n_counter_examples, [0, 1], xs_train, sz)
    logger.debug('Cached sampler is instantiated.')

    if method == 'hole':
        model = HolE(sz, latent_factors)
        trainer = StochasticTrainer(model, samplef=train_sampler.sample)
    elif method == 'transe':
        model = TransE(sz, latent_factors)
        trainer = PairwiseStochasticTrainer(model, samplef=train_sampler.sample)
    elif method == 'rescal':
        model = RESCAL(sz, latent_factors)
        trainer = StochasticTrainer(model, samplef=train_sampler.sample)
    else:
        msg = f'The model "{method}" is not supported.'
        logger.error(msg)
        raise ValueError(msg)

    # fit model to knowledge graph
    logger.debug('Starting to fit the model with training data.')
    train_start_time = time.process_time()
    trainer.fit(xs_train_triples, ys)
    logger.info(f'The training time took {time.process_time() - train_start_time} seconds')

    # get the model name
    model_name = get_model_prefix(kb.get_data_root(), method, latent_factors, n_counter_examples)

    # save model
    logger.debug(f'Saving the trained model to file {model_name}.pkl .')
    with open(model_name + '.pkl', 'wb') as output_file:
        pickle.dump(trainer.model, output_file)

    # save counter examples
    counter_examples_file = model_name + '_counterexamples.tsv'
    counter_examples = train_sampler.get_set_generated_counter_examples()
    logger.debug(f'Saving the counter examples to file {counter_examples_file} .')
    with open(counter_examples_file, 'w') as counter_ex_file:
        save_counter_examples(counter_examples, counter_ex_file)

    return trainer.model, counter_examples
