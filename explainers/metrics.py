from scipy.special import expit
from sklearn.metrics import log_loss
from sklearn.metrics import mean_squared_error, r2_score


# exponential iterator
def expit_mean_squared_error(y_true, y_pred, sample_weight=None,
                             multioutput='uniform_average'):
    return mean_squared_error(expit(y_true), y_pred,
                              sample_weight, multioutput)


def expit_r2(y_true, y_pred, sample_weight=None, multioutput='uniform_average'):
    return r2_score(expit(y_true), y_pred,
                    sample_weight=sample_weight, multioutput=multioutput)


def expit_log_loss(y_true, y_pred, eps=1e-15, normalize=True, sample_weight=None, labels=None):
    return log_loss(expit(y_true), expit(y_pred), eps=eps, normalize=normalize,
                    sample_weight=sample_weight, labels=labels)
