from __future__ import annotations

from functools import partial
from typing import Callable, List

import numpy as np
from scipy.special import expit

from data.kb import TRIPLE
from utils.types import Fact


def set_threshold(fn: Callable, threshold: float) -> Callable:
    """
    Application of the `threshold` argument to the function

    :param Callable fn: The function that have a threshold parameter
    :param float threshold: The value of the parameter that we want to apply to `fn`
    :return: The function `fn` with a set parameter `threshold`
    """
    return partial(fn, threshold=threshold)


def greater_than_threshold(explainer, instances, threshold=0.0):
    positive_predictions = []
    negative_predictions = []

    for instance in instances:
        ## instance[0] gets the tuple (subject, object, predicate)
        instance_score = explainer.get_score(instance[TRIPLE])
        if instance_score < threshold:
            negative_predictions.append(instance)
        else:
            positive_predictions.append(instance)

    return positive_predictions, negative_predictions, lambda x: 1 if x >= threshold else 0


def greater_than_median(explainer, instances, threshold=0.5):
    positive_predictions = []
    negative_predictions = []
    scores = []

    for instance in instances:
        ## instance[0] gets the tuple (subject, object, predicate)
        instance_score = explainer.get_score(instance[TRIPLE])
        scores.append(instance_score)

    median_value = np.percentile(scores, q=(threshold * 100))

    for instance in instances:
        instance_score = explainer.get_score(instance[TRIPLE])
        if instance_score <= median_value:
            negative_predictions.append(instance)
        else:
            positive_predictions.append(instance)

    return positive_predictions, negative_predictions, lambda x: 1 if x >= median_value else 0


def greater_than_expit_threshold(explainer: 'KBExplainer', instances: List[tuple[Fact, int]], threshold=0.5):
    positive_predictions = []
    negative_predictions = []

    for instance in instances:
        # instance[TRIPLE] gets the tuple (subject, object, predicate)
        instance_score = expit(explainer.get_score(instance[TRIPLE]))
        if instance_score < threshold:
            negative_predictions.append(instance)
        else:
            positive_predictions.append(instance)

    return positive_predictions, negative_predictions, lambda x: 1 if expit(x) >= threshold else 0
