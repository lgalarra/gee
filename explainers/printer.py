from typing import TextIO


def header(output_file_object: TextIO = None, delimiter: str = '\t'):
    """
    Provide header information for csv-like file

    if `output_file_object` is provided, the header will be written in that file
    if not, the header will be printed on STDOUT
    """
    text = "\t".join(['Triple/Relation', 'explanation-type', 'linear-real-accuracy', 'context-mse', 'target-mse',
                      'context-r2', 'context-mse-prob', 'target-mse-prob', 'context-r2-prob', 'context-accuracy',
                      'context-log-loss', 'context-auc-roc',
                      'rules-per-fact', 'total-rules', 'target-mr', 'context-mr', 'context-mrr', 'kendall-tau',
                      'train-positive-set',
                      'test-positive-set', 'train-negative-set',
                      'test-negative-set', 'explanation'])

    if output_file_object is not None:
        output_file_object.write(text + '\n')
    else:
        print(text)


def output_scores(translated_example, scores, explanation, file_wrapper: TextIO = None, delimiter: str = "\t"):
    result = "\t".join([" ".join(translated_example).strip(),
                        'rules-linear',
                        str(scores['linear-real-acc-score']),
                        str(scores['context-mse-score']),
                        str(scores['target-instance-mse-score']),
                        str(scores['context-r2']),
                        str(scores['context-mse-score-prob']),
                        str(scores['target-instance-mse-score-prob']),
                        str(scores['context-r2-prob']),
                        str(scores['context-acc-score']),
                        str(scores['context-log-loss']),
                        str(scores['context-auc-roc']),
                        str(scores['rules-per-fact']),
                        str(explanation._stats['total-rules']),
                        str(scores['mean-rank-target']),
                        str(scores['mean-rank-context']),
                        str(scores['mean-reciprocal-rank']),
                        str(scores['kendall-rank-correlation']),
                        str(explanation._stats['stats-positive-train-set']),
                        str(explanation._stats['stats-positive-test-set']),
                        str(explanation._stats['stats-negative-train-set']),
                        str(explanation._stats['stats-negative-test-set']),
                        explanation.output()
                        ])

    if file_wrapper is not None:
        file_wrapper.write(result + '\n')
    else:
        print(result)
