from __future__ import annotations

from typing import Callable, Optional

from sklearn.model_selection import train_test_split

from data.amie import AMIE
from data.kb import SUBJECT, OBJECT, PREDICATE, TRIPLE, TRUTH_VALUE, KB
from data.sample import ImprovedLCWASampler
from explainers.explanation import EmptyExplanation, LinearRuleBasedExplanation
from utils.knowledge_graph import group_triples_by_predicate, get_subject_and_object_neighbors, negate_predicate
from utils.logger import get_logger
from utils.types import Fact, KGEModel

logger = get_logger()


def get_kb_explainer(explainer_name: str = 'pca', model: KGEModel = None, kb: KB = None,
                     context_instances: [(Fact, int)] = None) -> Optional[KBExplainer]:
    """

    :param explainer_name:
    :param model:
    :param kb:
    :param context_instances:
    :return:
    """
    logger.info(f'Instantiate the "{explainer_name}" explainer.')
    if explainer_name == 'pca':
        return PCAKBExplainer(model, kb=kb, context_instances=context_instances)
    elif explainer_name == 'pca-full-no-constant-rules':
        return PCAFullNoConstantRulesKBExplainer(model, kb=kb, context_instances=context_instances)
    elif explainer_name == 'pca-full-rules':
        return PCAFullRulesKBExplainer(model, kb=kb, context_instances=context_instances)
    else:
        logger.error(f'No explainer found for the name "{explainer_name}".')
        return None


class KBExplainer:
    from explainers.discretization_functions import greater_than_median

    def __init__(self, model: KGEModel, kb: KB = None, context_instances=None):
        self.model = model
        self.kb = kb
        self.context_instances = context_instances

        # Prevent recalculation of known score for a given triplet
        self.cached_scores: dict[Fact, int] = {}

    def explain_instance(self, instance: Fact, positives_examples_fn: Callable = greater_than_median,
                         recursive_rules: bool = False):
        pass

    def explain_batch(self, instances, positives_examples_fn=greater_than_median, recursive_rules=False):
        pass

    def get_score(self, instance: Fact) -> float:
        """
        Get a score for a given instance triplet (s,o,p)
        The higher the score is, the higher is the probability of this triplet to be true.
        """
        if instance not in self.cached_scores:
            self.cached_scores[instance] = self.model._scores([instance[0]], [instance[2]], [instance[1]])
        return self.cached_scores[instance]


class PCAKBExplainer(KBExplainer):
    from explainers.discretization_functions import greater_than_median

    def __init__(self, model, kb=None, context_instances=None):
        super(PCAKBExplainer, self).__init__(model=model, kb=kb, context_instances=context_instances)
        self.predicate_map = group_triples_by_predicate(self.context_instances)
        self.atoms = 2
        self.constants_enabled = True
        self.constants_enforced = True
        self.min_support = 10

    def _amie(self, triples, minis, positive_predictions, translated_predicate, translated_negated_predicate):

        amie = AMIE()
        if minis > 0:
            rules = amie.mine(triples, const=self.constants_enabled, fconst=self.constants_enforced,
                              htr='"' + translated_predicate + "," + translated_negated_predicate + '"',
                              minpca=0.1, minc=0.0, minis=minis,
                              pm='support', bias='amie.mining.assistant.experimental.ILPMiningAssistant',
                              mins=min(self.min_support, minis), maxad=self.atoms, ranking_metric='pca-conf')
        else:
            # Use the standard confidence as metric
            htr_text = translated_predicate if len(positive_predictions) > 0 else translated_negated_predicate
            rules = amie.mine(triples, const=self.constants_enabled, fconst=self.constants_enforced,
                              htr='"' + htr_text + '"',
                              minpca=0.0, minc=0.1, minis=minis,
                              pm='support',
                              mins=self.min_support, maxad=self.atoms, ranking_metric='std-conf')
        return rules

    def _mine(self, positive_predictions, negative_predictions, predicate, recursive_rules=False):
        translated_predicate = self.kb.map_id_2_predicate(predicate)

        the_prefix = '' if not recursive_rules else '__'
        triples = self.convert_to_textual_triples(positive_predictions, prefix=the_prefix)
        triples.extend(self.convert_to_textual_triples(negative_predictions, negated=True, prefix=the_prefix))

        translated_negated_predicate = negate_predicate(translated_predicate)
        minis = min(len(positive_predictions), len(negative_predictions))
        self._prepare_background_kb(predicate, triples, recursive_rules=recursive_rules)

        rules = self._amie(triples, minis, positive_predictions, the_prefix + translated_predicate,
                           the_prefix + translated_negated_predicate)

        rules = sorted(rules, key=lambda x: x.confidence, reverse=True)
        max_rules = 500
        if len(rules) > max_rules:
            logger.info(f'Shortening the list to a maximum of {max_rules} rules.')
            rules = rules[:max_rules]

        return rules

    def explain_instance(self, instance: Fact, positives_examples_fn: Callable = greater_than_median,
                         recursive_rules: bool = False):
        logger.debug(f'Explaining instance {instance}')

        super(PCAKBExplainer, self).explain_instance(instance, positives_examples_fn, recursive_rules)

        # get a list of triples (subject, object, p) where p is always this instance's predicate.
        instance_context: [Fact] = self.predicate_map[instance[TRIPLE][PREDICATE]]

        # Now look for positive and negative examples for this triple
        neighbors = set()

        # Look for triples having the same subject & the same object
        s_neighbors, o_neighbors = get_subject_and_object_neighbors(instance, instance_context)
        neighbors.update(s_neighbors)
        neighbors.update(o_neighbors)

        vicinity_size = int(max(len(neighbors) / 3, 1))
        kb_size = self.kb.size()
        test_sampler_sub_obj = ImprovedLCWASampler(vicinity_size, [0, 1], instance_context, kb_size)

        neighbors.update(test_sampler_sub_obj.sample([instance]))
        positive_predictions, negative_predictions, threshold_fn = positives_examples_fn(self, neighbors)

        # Split here between train & test
        if len(positive_predictions) == 0 or len(negative_predictions) == 0:
            return EmptyExplanation()

        if len(positive_predictions) < 5:
            positive_predictions_train = list(positive_predictions)
            positive_predictions_test = list(positive_predictions)
        else:
            positive_predictions_train, positive_predictions_test = train_test_split(positive_predictions)

        if len(negative_predictions) < 5:
            negative_predictions_train = list(negative_predictions)
            negative_predictions_test = list(negative_predictions)
        else:
            negative_predictions_train, negative_predictions_test = train_test_split(negative_predictions)

        # Run rule mining
        rules = self._mine(positive_predictions_train, negative_predictions_train, instance[TRIPLE][PREDICATE],
                           recursive_rules=recursive_rules)
        logger.info(f'AMIE found {len(rules)} rules')

        if len(rules) == 0:
            return EmptyExplanation()

        local_context_positive, local_context_negative = self._prepare_context_for_explanation(
            positive_predictions_test,
            negative_predictions_test, recursive_rules=recursive_rules)

        local_training_context_positive, local_training_context_negative = \
            self._prepare_context_for_explanation(positive_predictions_train, negative_predictions_train,
                                                  recursive_rules=recursive_rules)

        # Find a place for the target instance
        p, n, f = positives_examples_fn(self, [instance])
        p, n = self._prepare_context_for_explanation(p, n, recursive_rules=recursive_rules)
        local_context_positive.extend(p)
        local_context_negative.extend(n)
        translated_target_instance = p[0] if len(p) > 0 else n[0]

        explanation = LinearRuleBasedExplanation(self.kb, self.context_instances,
                                                 local_context_positive, local_context_negative,
                                                 translated_target_instance,
                                                 rules,
                                                 local_training_context_positive + local_training_context_negative,
                                                 threshold_fn=threshold_fn)
        explanation._stats = {'stats-positive-train-set': len(positive_predictions_train),
                              'stats-negative-train-set': len(negative_predictions_train),
                              'stats-positive-test-set': len(positive_predictions_test),
                              'stats-negative-test-set': len(negative_predictions_test),
                              'total-rules': len(rules)}
        return explanation

    def explain_batch(self, instances, positives_examples_fn=greater_than_median, recursive_rules=False):
        super(PCAKBExplainer, self).explain_batch(instances)
        instance_context = self.predicate_map[instances[0][TRIPLE][PREDICATE]]

        # We need to get some counter-examples
        test_sampler_subject = ImprovedLCWASampler(1, [0], instance_context, self.kb.size())
        test_sampler_object = ImprovedLCWASampler(1, [1], instance_context, self.kb.size())
        test_sampler_sub_obj = ImprovedLCWASampler(1, [0, 1], instance_context, self.kb.size())
        neighbors = set()

        # Generate counter-examples for each of the instances in the batch
        for instance in instances:
            neighbors.update(test_sampler_subject.sample([instance]))
            neighbors.update(test_sampler_object.sample([instance]))
            neighbors.update(test_sampler_sub_obj.sample([instance]))

        neighbors.update(instances)

        positive_predictions, negative_predictions, threshold_fn = positives_examples_fn(self, neighbors)

        # Split here between train & test
        if len(positive_predictions) < 5 or len(negative_predictions) < 5:
            return EmptyExplanation()

        positive_predictions_train, positive_predictions_test = train_test_split(positive_predictions)
        negative_predictions_train, negative_predictions_test = train_test_split(negative_predictions)

        # Run rule mining
        rules = self._mine(positive_predictions_train, negative_predictions_train, instances[0][TRIPLE][PREDICATE])

        logger.info(f'AMIE found {len(rules)} rules')

        if len(rules) == 0:
            return EmptyExplanation()

        local_context_positive, local_context_negative = self._prepare_context_for_explanation(
            positive_predictions_test,
            negative_predictions_test)
        local_training_context_positive, local_training_context_negative = self._prepare_context_for_explanation(
            positive_predictions_train,
            negative_predictions_train)

        explanation = LinearRuleBasedExplanation(self.kb, self.context_instances,
                                                 local_context_positive, local_context_negative,
                                                 None, rules,
                                                 local_training_context_positive + local_training_context_negative,
                                                 threshold_fn=threshold_fn)
        explanation._stats = {'stats-positive-train-set': len(positive_predictions_train),
                              'stats-negative-train-set': len(negative_predictions_train),
                              'stats-positive-test-set': len(positive_predictions_test),
                              'stats-negative-test-set': len(negative_predictions_test),
                              'total-rules': len(rules)}

        return explanation

    def _prepare_context_for_explanation(self, positive_predictions, negative_predictions, recursive_rules=False):
        context_positive = []
        context_negative = []
        the_prefix = '__' if not recursive_rules else ''
        for p_pred in positive_predictions:
            context_positive.append((self.convert_to_textual_triple(p_pred, prefix=the_prefix), p_pred[TRUTH_VALUE],
                                     self.get_score(p_pred[TRIPLE])))

        for n_pred in negative_predictions:
            context_negative.append(
                (self.convert_to_textual_triple(n_pred, negated=True, prefix=the_prefix), n_pred[TRUTH_VALUE],
                 self.get_score(n_pred[TRIPLE])))

        return context_positive, context_negative

    def _prepare_background_kb(self, target_predicate, output, recursive_rules=False):
        if recursive_rules:
            for predicate in self.predicate_map:
                for triple in self.predicate_map[predicate]:
                    output.append(self.kb.convert_to_textual_triple(triple))
        else:
            for predicate in self.predicate_map:
                if predicate != target_predicate:
                    for triple in self.predicate_map[predicate]:
                        output.append(self.kb.convert_to_textual_triple(triple))

    # This method assumes the instances have the form (triple, truth-value)
    def get_positive_and_negative_predictions(self, target_instances):
        positive_predictions = []
        negative_predictions = []

        for instance in target_instances:
            # instance[0] gets the tuple (subject, object, predicate)
            instance_score = self.get_score(instance[TRIPLE])
            if instance_score <= 0.0:
                negative_predictions.append(instance)
            else:
                positive_predictions.append(instance)

        return positive_predictions, negative_predictions

    def convert_to_textual_triples(self, triples, negated=False, prefix=''):
        converted_triples = []
        for triple in triples:
            converted_triples.append(self.convert_to_textual_triple(triple, negated, prefix=prefix))

        return converted_triples

    def convert_to_textual_triple(self, triple, negated=False, prefix=''):
        sid = triple[TRIPLE][SUBJECT]
        oid = triple[TRIPLE][OBJECT]
        pid = triple[TRIPLE][PREDICATE]
        return (self.map_id_2_constant(sid),
                prefix + self.map_id_2_predicate(pid, negated=negated),
                self.map_id_2_constant(oid))

    def map_id_2_predicate(self, pid, negated=False):
        if self.kb is not None:
            return self.kb.map_id_2_predicate(pid, negated)
        else:
            if negated:
                return '<predicate_neg_' + str(pid) + '>'
            else:
                return '<predicate_' + str(pid) + '>'

    def map_id_2_constant(self, vid):
        if self.kb is not None:
            return self.kb.id2entity[vid]
        else:
            return '<constant_' + str(vid) + '>'


class PCAFullNoConstantRulesKBExplainer(PCAKBExplainer):

    def __init__(self, model, kb=None, context_instances=None):
        super(PCAFullNoConstantRulesKBExplainer, self).__init__(model, kb=kb, context_instances=context_instances)
        self.atoms = 3
        self.constants_enabled = False
        self.constants_enforced = False


class PCAFullRulesKBExplainer(PCAKBExplainer):

    def __init__(self, model, kb=None, context_instances=None):
        super(PCAFullRulesKBExplainer, self).__init__(model, kb=kb, context_instances=context_instances)
        self.atoms = 3
        self.constants_enabled = True
        self.constants_enforced = False
        self.min_support = 10

    def _amie(self, triples, minis, positive_predictions, translated_predicate, translated_negated_predicate):
        amie = AMIE()
        if minis > 0:
            rules = amie.mine(triples, const=self.constants_enabled, fconst=self.constants_enforced,
                              htr='"' + translated_predicate + "," + translated_negated_predicate + '"',
                              minpca=0.1, minc=0.0, minis=minis,
                              pm='support',
                              bias='amie.mining.assistant.experimental.ILPMiningAssistantShortConstantRules',
                              mins=self.min_support, maxad=self.atoms, ranking_metric='pca-conf')
        else:
            # Use the standard confidence as metric
            htr_text = translated_predicate if len(positive_predictions) > 0 else translated_negated_predicate
            rules = amie.mine(triples, const=self.constants_enabled, fconst=self.constants_enforced,
                              htr='"' + htr_text + '"',
                              minpca=0.0, minc=0.1, minis=minis,
                              pm='support',
                              mins=self.min_support, maxad=self.atoms, ranking_metric='std-conf')
        return rules
