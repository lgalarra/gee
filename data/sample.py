import random
from copy import deepcopy
from typing import Union, List

from skge.sample import LCWASampler

from data.kb import TRIPLE, SUBJECT, OBJECT, PREDICATE
from utils.logger import get_logger
from utils.types import Fact

logger = get_logger()


class ImprovedLightweightLCWASampler(LCWASampler):
    """
    Sample negative examples according to the local closed world assumption.
    This implementation makes sure negative examples respect the schema constraints
    of the predicate, e.g., if the predicate is place of birth, negative examples
    will involve pairs person - city.
    """

    def __init__(self, n, modes, xs, sz):
        triples_xs = [x[TRIPLE] for x in xs]
        super(ImprovedLightweightLCWASampler, self).__init__(n, modes, triples_xs, sz)
        self.soindex = {}
        for s, o, p in triples_xs:
            if p not in self.soindex:
                self.soindex[p] = ([], [])

            self.soindex[p][0].append(s)
            self.soindex[p][1].append(o)

    def _sample(self, triple, mode):
        nex = list(deepcopy(triple))
        res = None
        predicate = nex[2]
        random.shuffle(self.soindex[predicate][mode])

        nex[mode] = self.soindex[predicate][mode][0]
        if self.counts[(nex[0], nex[2])] > 0 and tuple(nex) not in self.xs:
            res = (tuple(nex), -1.0)

        return res

    def sample(self, xys):
        res = []
        if isinstance(self.n, int):
            for x, _ in xys:
                for _ in range(self.n):
                    for mode in self.modes:
                        t = self._sample(x, mode)
                        if t is not None:
                            res.append(t)
        elif isinstance(self.n, float):
            if self.n <= 0.0 or self.n > 1.0:
                raise ValueError('The sampling probability must be between 0 and 1')
            else:
                for x, _ in xys:
                    prob = random.random()
                    if prob <= self.n:
                        for mode in self.modes:
                            t = self._sample(x, mode)
                            if t is not None:
                                res.append(t)

        return res


class ImprovedLCWASampler(LCWASampler):
    """
    Sample negative examples according to the local closed world assumption.
    This implementation makes sure negative examples respect the schema constraints
    of the predicate, e.g., if the predicate is place of birth, negative examples
    will involve pairs person - city.
    """

    def __init__(self, n: int, modes: [int], xs: [(Fact, int)], sz: (int, int, int)):
        """
        :param int n: vicinity size
        :param list modes:
            - [0]   :
            - [1]
            - [O,1] : 0 & 1 together
        :param [(Fact, int)] xs: The array of tuples and their truth score
        :param (int,int,int) sz: The size of the KG xs : number of entities and number of predicates (e,e,p).
        """
        logger.debug(f'Instantiate new Sampler')

        # extract Fact from xs
        triples_xs = [x[TRIPLE] for x in xs]

        # call super (useless)
        super(ImprovedLCWASampler, self).__init__(n, modes, triples_xs, sz)

        # The key of `so_index` is a predicate
        # The
        self.so_index: dict[int, (set[int], set[int])] = {}
        self.i_op_index = {}
        self.i_sp_index = {}
        self.seen_triples = {}
        for s, o, p in triples_xs:
            if p not in self.so_index:
                self.so_index[p] = (set(), set())

            if 0 in self.modes:
                self.so_index[p][0].add(s)
                if (o, p) not in self.i_op_index:
                    self.i_op_index[(o, p)] = set()
                self.i_op_index[(o, p)].add(s)

            if 1 in self.modes:
                self.so_index[p][1].add(o)

                if (s, p) not in self.i_sp_index:
                    self.i_sp_index[(s, p)] = set()

                self.i_sp_index[(s, p)].add(o)

        # print('Indexing time for sampling', time.process_time() - st, 'seconds')

    def _sample(self, triple, mode):
        res = None
        s = triple[SUBJECT]
        p = triple[PREDICATE]
        o = triple[OBJECT]
        if triple not in self.seen_triples:
            self.seen_triples[triple] = {}

        if mode not in self.seen_triples[triple]:
            if mode == 0:
                self.seen_triples[triple][mode] = [0, list(self.so_index[p][mode].difference(
                    self.i_op_index[(o, p)] if (o, p) in self.i_op_index else set()))]
            else:
                self.seen_triples[triple][mode] = [0, list(self.so_index[p][mode].difference(
                    self.i_sp_index[(s, p)] if (s, p) in self.i_sp_index else set()))]

        nextidx = self.seen_triples[triple][mode][0]
        if nextidx < len(self.seen_triples[triple][mode][1]):
            nex = list(deepcopy(triple))
            nex[mode] = self.seen_triples[triple][mode][1][nextidx]
            if tuple(nex) != triple:
                self.seen_triples[triple][mode][0] += 1
                res = (tuple(nex), -1)

        return res


class ImprovedCachedLCWASampler(ImprovedLightweightLCWASampler):
    """
    Sample negative examples according to the local closed world assumption.
    This implementation makes sure negative examples respect the schema constraints
    of the predicate, e.g., if the predicate is place of birth, negative examples
    will involve pairs person - city.
    """

    def __init__(self, n: Union[int, float], modes: List[int], xs: List[tuple[Fact, int]], sz: (int, int, int)):
        super(ImprovedCachedLCWASampler, self).__init__(n, modes, xs, sz)
        self.cache = set()

    def _sample(self, triple, mode):
        res = super(ImprovedCachedLCWASampler, self)._sample(triple, mode)
        if res is not None:
            self.cache.add(res)

        return res

    def get_set_generated_counter_examples(self) -> set:
        return self.cache
