import csv
import subprocess
import tempfile

from utils.logger import get_logger

logger = get_logger()


class Rule:

    def __init__(self, content, support, confidence):
        self.content = content
        self.support = support
        self.confidence = confidence

    def __str__(self):
        return '{' + self.content + ', support: ' + str(self.support) + ', confidence: ' + str(self.confidence) + '}'

    def __repr__(self):
        return self.__str__()

    def to_tuple(self):
        return self.content, str(self.support), str(self.confidence)


def timeout_hnd(signum, frame):
    raise Exception('AMIE timed out')


class AMIE:
    def __init__(self):
        pass

    def _parse_args(self, kwargs, join=True):
        strs = []
        for key, value in kwargs.items():
            # AMIE does not consider the ranking metric
            if key == 'ranking_metric':
                continue

            if type(value) == bool and value:
                strs.append('-' + key)
            elif type(value) == bool and not value:
                continue
            else:
                if join:
                    strs.append('-' + key + ' ' + str(value))
                else:
                    strs.append('-' + key)
                    strs.append(str(value))

        if join:
            return " ".join(strs) + " "
        else:
            return strs

    def _parse_rules(self, text, ranking_metric='pca-conf'):
        lines = text.split('\n')
        rules = []
        for line in lines:
            if line.startswith('?'):
                line_components = line.split('\t')
                if len(line_components) >= 5:
                    rule_content = line_components[0]
                    rule_confidence = line_components[2] if ranking_metric == 'std-conf' \
                        else line_components[3]
                    rule_support = line_components[4]
                    rule = Rule(rule_content, rule_support, rule_confidence)
                    rules.append(rule)

        return rules

    def mine(self, triples, **kwargs):
        ranking_metric = 'pca-conf' if 'ranking_metric' not in kwargs else kwargs['ranking_metric']
        rules = []
        with tempfile.NamedTemporaryFile(mode='w+') as tfile:
            csvfile = csv.writer(tfile, delimiter='\t')
            for triple in triples:
                csvfile.writerow(triple)

            tfile.seek(0)

            # Mine
            args = ['java', '-jar', 'lib/amie3.jar']
            args.extend(self._parse_args(kwargs, join=False))
            args.append(tfile.name)
            logger.debug(f'Java arguments from AMIE : "{args}".')

            completed_process = None
            try:
                completed_process = subprocess.run(args=args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                                   timeout=300, text=True)
            except Exception as exc:
                print(exc)

            if completed_process is not None:
                rules = self._parse_rules(completed_process.stdout, ranking_metric)

        # We will parse the rules afterwards
        return rules
