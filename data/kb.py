#!/usr/bin/python3
from __future__ import annotations  # allow type hinting hoisting

import csv
import os
from typing import List

from utils.logger import get_logger
from utils.types import Fact

logger = get_logger()

SUBJECT = 0
OBJECT = 1
PREDICATE = 2

TRIPLE = 0
TRUTH_VALUE = 1
MODEL_ANSWER = 2


def get_training_kb(folder: os.path) -> KB:
    train_filename = os.path.join(folder, 'train.tsv')
    logger.debug(f'Creating KB object with file {train_filename}.')
    return KB(train_filename)


class KB:

    def __init__(self, kb_path: os.path, delimiter: str = '\t'):

        # the path to the file containing the KG
        self.kb_path: os.path = kb_path

        # the name of the used dataset
        self.dataset_name = os.path.dirname(kb_path).split('/')[-1]

        # correspondence between the entity and their id
        self.entity2id: dict[str, id] = {}

        # correspondence between the predicate and their id
        self.predicate2id: dict[str, id] = {}

        # correspondence between the entities' id and their value
        self.id2entity: dict[id, str] = {}

        # correspondence between the predicates' id and their value
        self.id2predicate: dict[int, str] = {}

        # number of different entities, used to generate entity ids
        self.entity_counter: int = 0

        # number of different predicates, used to generate predicate ids
        self.predicate_counter: int = 0

        # the delimiter used while parsing kg file
        self.delimiter: str = delimiter

        # array of all the triplets (subject, object, predicate) with an additional information
        self.triples: List[(Fact, int)] = []

    def get_data_root(self) -> os.path:
        return os.path.dirname(self.kb_path)

    def _map_entity(self, entity: str) -> int:
        """
        Check if this entity is already known by the KG.
        If so, just return the id of this entity.
        If not, give an id to this entity and return it.
        """
        if entity not in self.entity2id:
            self.entity2id[entity] = self.entity_counter
            self.id2entity[self.entity_counter] = entity
            self.entity_counter += 1

        return self.entity2id[entity]

    def _map_predicate(self, predicate: str) -> int:
        """
        Check if this predicate is already known by the KG.
        If so, just return the id of this predicate.
        If not, give an id to this predicate and return it.
        """
        if predicate not in self.predicate2id:
            self.predicate2id[predicate] = self.predicate_counter
            self.id2predicate[self.predicate_counter] = predicate
            self.predicate_counter += 1

        return self.predicate2id[predicate]

    def fit(self) -> (int, int, List[(Fact, int)], dict[int, str], dict[int, str]):
        """
        Read the file stored in `self.kb_path` and extract triplets (subject, object, predicate)
        The returned tuple contains the following information:
            - number of different entities
            - number of different predicates
            - array of all the triplets (subject, object, predicate) with +1 if the statement if True and -1 if False
            - dictionary of correspondence id -> entity
            - dictionary of correspondence id -> predicate
        """
        logger.info(f'Loading file {self.kb_path} in KB.')
        with open(self.kb_path) as csv_file:
            read_csv = csv.reader(csv_file, delimiter=self.delimiter)
            for row in read_csv:
                subject = self._map_entity(row[0])
                predicate = self._map_predicate(row[1])
                object = self._map_entity(row[2].rstrip('.'))
                if len(row) > 3:
                    self.triples.append(((subject, object, predicate), int(row[3])))
                else:
                    self.triples.append(((subject, object, predicate), 1))
        logger.debug(f'KB information : '
                     f'#entities : {len(self.entity2id)}, '
                     f'#predicates : {len(self.predicate2id)}, '
                     f'#relations : {len(self.triples)}.')
        return len(self.entity2id), len(self.predicate2id), self.triples, self.id2entity, self.id2predicate

    def get_info(self) -> (int, int, List[(Fact, int)], dict[int, str], dict[int, str]):
        """
        The returned tuple contains the following information:
            - number of different entities
            - number of different predicates
            - array of all the triplets (subject, object, predicate) with +1 if the statement if True and -1 if False
            - dictionary of correspondence id -> entity
            - dictionary of correspondence id -> predicate
        """
        return len(self.entity2id), len(self.predicate2id), self.triples, self.id2entity, self.id2predicate

    def fit_transform(self, input_file: os.PathLike) -> [(Fact, int)]:
        """
        Load triplets from input file.
        Basically mimics the `fit` function.
        The unknown entities and relations are ignored.

        :param os.PathLike input_file: The path where the file is stored
        :return: The facts with their truth value
        """
        triples = []
        logger.debug(f'Loading file {input_file} to extract facts.')

        with open(input_file) as csv_file:
            count_lines = 0
            read_csv = csv.reader(csv_file, delimiter=self.delimiter)
            for row in read_csv:
                count_lines += 1
                subject = row[0]
                predicate = row[1]
                object = row[2].rstrip('.')
                # Do not consider triples containing unseen entities
                if subject in self.entity2id and predicate in self.predicate2id and object in self.entity2id:
                    id_subject = self.entity2id[row[0]]
                    id_predicate = self.predicate2id[row[1]]
                    id_object = self.entity2id[row[2].rstrip('.')]
                    if len(row) > 3:
                        triples.append(((id_subject, id_object, id_predicate), int(row[3])))
                    else:
                        triples.append(((id_subject, id_object, id_predicate), 1))
        logger.info(f'Facts loaded at {float(len(triples)) / float(count_lines) * 100:.2f}% from file {input_file}.')
        return triples

    def convert_to_textual_triple(self, triple):
        sid = triple[TRIPLE][SUBJECT]
        oid = triple[TRIPLE][OBJECT]
        pid = triple[TRIPLE][PREDICATE]
        return (self.map_id_2_constant(sid),
                self.map_id_2_predicate(pid, negated=(triple[TRUTH_VALUE] != 1)),
                self.map_id_2_constant(oid))

    def map_id_2_predicate(self, pid: int, negated: bool = False) -> str:
        predicate = self.id2predicate[pid]
        if negated:
            if predicate.startswith('<'):
                return predicate.replace('<', '<neg_')
            else:
                return 'neg_' + predicate
        else:
            return self.id2predicate[pid]

    def map_id_2_constant(self, vid: int) -> str:
        return self.id2entity[vid]

    def size(self) -> (int, int, int):
        return len(self.entity2id), len(self.entity2id), len(self.predicate2id)
