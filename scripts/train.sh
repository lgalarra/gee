#!/usr/bin/env bash

. /etc/profile.d/modules.sh

set -xv


module load spack/python/3.7.6
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py fb15k-237-wcp transe 50 1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py fb15k transe 50 1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py kinship hole 20 1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py kinship rescal 20 1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py kinship transe 20 1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py wn18 hole 20 1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py nations rescal 20 1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py nations transe 20 1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py nell186 hole 50 1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py nell186 rescal 50 1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py nell186 transe 50 1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py yago3-10 hole 150 0.1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py yago3-10 rescal 50 0.1
PYTHONPATH=/udd/lgalarra/.local/lib/python3.7/site-packages python3 train.py yago3-10 transe 50 0.1

